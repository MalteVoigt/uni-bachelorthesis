package presentation;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import layout.TableLayout;

@SuppressWarnings("serial")
public class ConsolePanel extends JPanel {
	
	public JTextField userNameField;
	
	private JLabel userNameLabel;
	
	public JPasswordField userPasswordField;
	
	private JLabel userPasswordLabel;
	
	private StyledDocument styledDocument;
	
	private SimpleAttributeSet attributes;

	private JScrollPane scrollPane;
	

	
	public ConsolePanel(){
		init();
	}
	
	
	
	private void init(){
		TableLayout layout = new TableLayout(new double[][] {{45, 130, 130, 45}, {540, 15, 30, 30}});
		setLayout(layout);
		
		StyleContext context = new StyleContext();
        
        styledDocument = new DefaultStyledDocument(context);
        
        attributes = new SimpleAttributeSet();

        Style style = context.getStyle(StyleContext.DEFAULT_STYLE);
        StyleConstants.setAlignment(style, StyleConstants.ALIGN_LEFT);
        StyleConstants.setFontSize(style, 12);
        StyleConstants.setBold(attributes, true);
        StyleConstants.setItalic(attributes, true);
        
        JTextPane textPane = new JTextPane(styledDocument);
        textPane.setEditable(false);
        textPane.setBackground(Color.LIGHT_GRAY);
        
        scrollPane = new JScrollPane(textPane);
        
        userNameField = new JTextField();
        userNameLabel = new JLabel("Benutzername:");
        
        userPasswordField = new JPasswordField();
        userPasswordLabel = new JLabel("Passwort: ");
        
        
        
        //############ DEMO ############
        userNameField.setText("malte");
        userPasswordField.setText("test");
        //############ DEMO ############
        
        
        
		add(scrollPane, "0,0,3,0");
		add(userNameLabel, "1,2");
		add(userNameField, "2,2");
		add(userPasswordLabel, "1,3");
		add(userPasswordField, "2,3");
	}
	
	
	public final void printMessage(String _message){
		try{
			styledDocument.insertString(styledDocument.getLength(), _message + "\n", attributes);
			
			scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getMaximum());
			scrollPane.repaint();
	    	
	    } catch (BadLocationException e) {}
	}
	
}

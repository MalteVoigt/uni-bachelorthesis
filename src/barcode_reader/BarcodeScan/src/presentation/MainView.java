package presentation;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import layout.TableLayout;

@SuppressWarnings("serial")
public class MainView extends JFrame {
	
	private ConsolePanel consolePanel;
	
	
	
	public MainView(){
		new SysTray(this);
		initGUI();
	}
	
	
	
	private void initGUI(){		
		setSize(350, 680);
		setResizable(false);
		setTitle("Scan-Client - Konsole");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		addWindowListener(windowListener());
		
		Image img = Toolkit.getDefaultToolkit().getImage("images/barcode-reader-icon-128.png");
		setIconImage(img);
			
		TableLayout layout = new TableLayout(new double[][] {{350}, {680}});
		setLayout(layout);
		
		consolePanel = new ConsolePanel();
		
		add(consolePanel, "0,0");
	
		setVisible(true);
	}
	
	
	private WindowListener windowListener(){
		return new WindowListener(){
			public void windowIconified(WindowEvent e) {
				setVisible(false);
				dispose();	
			}
			
			public void windowClosing(WindowEvent e) {}
			public void windowOpened(WindowEvent e) {}
			public void windowClosed(WindowEvent e) {}
			public void windowDeiconified(WindowEvent e) {}
			public void windowActivated(WindowEvent e) {}
			public void windowDeactivated(WindowEvent e) {}
		};
	}
	
	
	public void printMessage(String _message){
		consolePanel.printMessage(_message);
	}
	
	
	public String getUsername(){				
		return consolePanel.userNameField.getText();
	}
	
	
	public String getPassword(){
		return String.valueOf((consolePanel.userPasswordField.getPassword()));	
	}
	
}

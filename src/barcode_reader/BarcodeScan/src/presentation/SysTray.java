package presentation;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class SysTray {
	
	private MainView mainView;
	
	private TrayIcon trayIcon;
	
	private PopupMenu popupMenu;

	private MenuItem console;
	private MenuItem exit;
	
	

	public SysTray(MainView _mainView){
		mainView = _mainView;
		
		Image img = Toolkit.getDefaultToolkit().getImage("images/barcode-reader-icon-16.png");
		
		trayIcon = new TrayIcon(img, "Scan-Client - Konsole", createPopupMenu());
		trayIcon.addActionListener(showConsoleActionListener());

        try {
			SystemTray.getSystemTray().add(trayIcon);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}
	

	
	private PopupMenu createPopupMenu() throws HeadlessException {	 
		popupMenu = new PopupMenu();

		console = new MenuItem("Scan-Client - Konsole");
		exit = new MenuItem("Beenden");

		console.addActionListener(showConsoleActionListener()); 
        exit.addActionListener(exitActionListener()); 

        popupMenu.add(console);
        popupMenu.add(exit);
	 
	    return popupMenu;
    }

	
	private ActionListener showConsoleActionListener(){
		return new ActionListener() {	 
           public void actionPerformed(ActionEvent e) {	 
        	   mainView.setVisible(true);
           }	 
        };
	}
	
	
	private ActionListener exitActionListener(){
		return new ActionListener() { 
           public void actionPerformed(ActionEvent e) { 
               System.exit(0); 
           } 
        };
	}

}
package logic;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.TooManyListenersException;

import network.Communication;


public class PortReader implements Runnable, SerialPortEventListener {
    
    public static final String defaultPort = "COM3";
	
    public static CommPortIdentifier portId;
	
    public static Enumeration portList;
    
    private InputStream inputStream;
    
    private SerialPort serialPort;
    
    private Thread readerThread;
    
    private Thread controllerThread;

	private BarcodeClient barcodeClient;
    
    private PortController portController;
    
   
    
    public PortReader(BarcodeClient _barcodeClient) {
    	barcodeClient = _barcodeClient;
    	portController = new PortController(this);
    	
    	init();
    }

    
    private void init(){
    	barcodeClient.printMessage("Initialisiere Scan Modul...");
    	
    	if(idle()){
	    	barcodeClient.printSeparator();
			try {
			    serialPort = (SerialPort) portId.open("BarcodeReader", 2000);
			    serialPort.addEventListener(this);
				serialPort.notifyOnDataAvailable(true);
			    serialPort.setSerialPortParams(9600, 
			    							   SerialPort.DATABITS_8, 
											   SerialPort.STOPBITS_1, 
											   SerialPort.PARITY_NONE);

			    inputStream = serialPort.getInputStream();
			    
			} catch (PortInUseException e) {
				System.out.println("Port " + defaultPort + " already in use: " + e.getMessage());
			}
			  catch (TooManyListenersException e) {
				System.out.println("Error while adding EventListener: " + e.getMessage());
			} 
			  catch (UnsupportedCommOperationException e) {
				System.out.println("Unsupported Operation: " + e.getMessage());
			}
			  catch (IOException e) {
				System.out.println("Error while initializing InputStream: " + e.getMessage());
			}
		
			readerThread = new Thread(this);		
			readerThread.start();
			
			controllerThread = new Thread(portController);
			controllerThread.start();
    	}
    }
        
    
    private boolean idle(){
    	while(true){
			portList = CommPortIdentifier.getPortIdentifiers();
		
			while (portList.hasMoreElements()) {
			    portId = (CommPortIdentifier) portList.nextElement();
			    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
					if (portId.getName().equals(defaultPort)) {
						barcodeClient.printMessage("Scan Modul initialisiert: " 
													+ defaultPort);
					    return true;
					} 
			    } 
			}  
    	}
    }
    

    public void serialEvent(SerialPortEvent event) {
		switch (event.getEventType()) {		
			case SerialPortEvent.DATA_AVAILABLE:
			    byte[] readBuffer = new byte[20];
	
			    try {
					while (inputStream.available() > 0) {
					    int numBytes = inputStream.read(readBuffer);
					}		
					
					String barcode = new String(readBuffer).trim();
					
					barcodeClient.printMessage("Barcode gescannt: " + barcode);
					
					barcodeClient.communication.createRequest(barcode);

					barcodeClient.printSeparator();
					
			    } catch (IOException e) {}	
			    break;
			default:
				break;
		}
    } 
    
    
    public void interrupt(){
    	barcodeClient.printMessage("Leseger�t getrennt!");
    	
    	try {
        	serialPort.close();
			inputStream.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}

    	readerThread.interrupt();
    	init();
    }

    
    public void run() {
		try {
		    Thread.sleep(20000);		    
		} catch (InterruptedException e) {}
    }  
	
}



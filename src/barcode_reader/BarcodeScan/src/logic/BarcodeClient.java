package logic;

import network.Communication;
import network.ResponseHandler;
import presentation.MainView;
import presentation.SysTray;

public class BarcodeClient {
	
	private MainView mainView;
	
	private ResponseHandler responseHandler;
	
	public Communication communication;
	
	private PortReader portReader;
	

	public BarcodeClient(){
		mainView = new MainView();
		responseHandler = new ResponseHandler(this);
		communication = new Communication(this, responseHandler);
		portReader = new PortReader(this);
	}

	
	
	public void printMessage(String _message){
		mainView.printMessage(_message);
	}
	
	
	public void printSeparator(){
		mainView.printMessage("-----------------------------------------");
	}
	
	
	public String getUsername(){				
		return mainView.getUsername();
	}
	
	
	public String getPassword(){
		return mainView.getPassword();		
	}
	

    public static void main(String[] args) {
    	new BarcodeClient();
    }
	
}

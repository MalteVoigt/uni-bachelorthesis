package logic;

import gnu.io.CommPortIdentifier;

public class PortController implements Runnable {
	
	private PortReader portReader;

	
	
	public PortController(PortReader _portReader){
		portReader = _portReader;
	}
	
	
	
	@SuppressWarnings("static-access")
	public void watch(){
		boolean isConnected = true;
		
    	while(isConnected){
    		isConnected = false;
    		
    		portReader.portList = CommPortIdentifier.getPortIdentifiers();
		
			while (portReader.portList.hasMoreElements()) {
				portReader.portId = (CommPortIdentifier) portReader.portList.nextElement();
			    if (portReader.portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
					if (portReader.portId.getName().equals(portReader.defaultPort)) {
						isConnected = true;
					} 
			    } 
			}  
    	}
    	portReader.interrupt();
    }
	

	@Override
	public void run() {
		watch();
		try {
		    Thread.sleep(20000);		    
		} catch (InterruptedException e) {}
	}
	
}

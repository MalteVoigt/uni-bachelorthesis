package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import logic.BarcodeClient;

public class Communication {
	
	private BarcodeClient barcodeClient;
	
	private ResponseHandler responseHandler;
	
	private String url = "https://syncreal.uni-bremen.de/barcode/index.php?";
	
	private String username;
	
	private String password;
	

	
	public Communication(BarcodeClient _barcodeClient, ResponseHandler _responseHandler){
		barcodeClient = _barcodeClient;
		responseHandler = _responseHandler;
	}
	
	
	
	public void createRequest(String _barcode){
		if(validate()){
			barcodeClient.printMessage("Sende Anfrage. Bitte warten...");
			
			url += "user=" + username + "&";
			url += "pw=" + createMD5(password) + "&";
			url += "barcode=" + _barcode + "&";
			url += "action=" + "addProductToList";
			
			sendRequest(url);
		}
	}
	
	
	private boolean validate(){	
		username = barcodeClient.getUsername();
		password = barcodeClient.getPassword(); 
		
		if(username.equals("") || password.equals("")){
			barcodeClient.printMessage("Bitte Benutzername und Passwort eingeben!");
			return false;
		} 
		return true;
	}
	
	
	private void sendRequest(String _url){
		URL url;
		
		try {
			url = new URL(_url);		
        
	        URLConnection connection = url.openConnection();
	        
	        BufferedReader in = new BufferedReader(
	        						new InputStreamReader(
	        							connection.getInputStream()));
	        	        
	        String inputLine;
	        String response = "";
	
	        while ((inputLine = in.readLine()) != null){
	        	response += inputLine;
	        }
	            
	        in.close();

	        responseHandler.handleResponse(response);
        
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private String createMD5(String _string){
		MessageDigest md5;
		byte[] hash = null;
		
		try {
			md5 = MessageDigest.getInstance("MD5");
	        md5.reset();
	        md5.update(_string.getBytes());
	        hash = md5.digest();
	        
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		        
        StringBuffer hexString = new StringBuffer();
        
        for (int i = 0; i < hash.length; i++) {
		   	String hex = Integer.toHexString(0xFF & hash[i]); 
		   	
		   	if(hex.length()==1){
		   		hexString.append('0');
		   	}		   	
		   	hexString.append(hex);
	    }
        
		return hexString.toString();
	}
	
}

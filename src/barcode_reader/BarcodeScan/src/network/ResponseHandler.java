package network;

import com.google.gson.Gson;

import logic.BarcodeClient;

public class ResponseHandler {
	
	private BarcodeClient barcodeClient;
	
	

	public ResponseHandler(BarcodeClient _barcodeClient){
		barcodeClient = _barcodeClient;
	}
	
	
	
	public void handleResponse(String _response){			
		Gson gson = new Gson();
		String s = gson.fromJson(_response, String.class);				
		barcodeClient.printMessage(s);
	}
	
}

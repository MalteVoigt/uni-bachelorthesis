<?php
	function handleRequest($_user, $_password, $_barcode, $_action){	
		switch($_action){
			case("authenticate");
				if(authenticateUser($_user, $_password)){
					$requestResult = "true";
				} else { 
					$requestResult = "Benutzername oder Passwort falsch!";
				}
				break;
				
			case("getProductDetails"):
				$requestResult = getDetailedInformation($_barcode);
				break;
				
			case("addProductToList"):	
				$requestResult = "Benutzername oder Passwort falsch!";
				
				if(authenticateUser($_user, $_password)){
					$userId = getUserId($_user);
					
					if($userId != -1){
						$content = getRawData($_barcode);
						
						if(productExists($content)){
							$productName = getProductName($content);
							$subTitle = getsubTitle($content);
							$productAmount = getProductAmount($content);
							$requestResult = addProductToList($userId, $_barcode, $productName, $subTitle, $productAmount);
						} else {
							$requestResult = "Fehler: Produkt nicht gefunden!";
						}
					}
				}
				break;
				
			case("deleteProductFromList"):
				$requestResult = "Benutzername oder Passwort falsch!";
				
				if(authenticateUser($_user, $_password)){
					$userId = getUserId($_user);
					
					if($userId != -1){
						$requestResult = deleteProductFromList($userId, $_barcode);
					}
				}
				break;
				
			case("getShoppingList"):
				$requestResult = array();
			
				if(authenticateUser($_user, $_password)){
					$userId = getUserId($_user);
					
					if($userId != -1){
						$requestResult = getShoppingList($userId);
					}
				}
				break;
				
			default:
				$requestResult = "Fehler: Undefinierter Methodenaufruf.";
				break;
		}
		
		return $requestResult;
	}
	
	
	function getRawData($_barcode){
		$url = 'http://www.codecheck.info/product.search?q='.$_barcode.'&OK=Suchen';
		
		$handle = fopen($url, "r");
		$content = '';
		
		while (!feof($handle)) {
			$content .= fread($handle, 8192);
		}
		fclose($handle);
		
		return $content;
	}
?>
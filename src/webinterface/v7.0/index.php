<?php
	include "config.php";
	include "communication.php";
	include "dboperations.php";
	include "barcoo.php";
	include "gepir.php";
	
	mysql_set_charset("utf8", $db_link);
	
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST'){	
		$username = $_POST['username'];
		$password = $_POST['password'];
		$barcode = $_POST['barcode'];
		$name = $_POST['name'];
		$isDrink = $_POST['isDrink'];
		$fat = $_POST['fat'];
		$satFat = $_POST['satFat'];
		$sugar = $_POST['sugar'];
		$salt = $_POST['salt'];
		$kcal = $_POST['energy'];
		$image = $_POST["image"];
		$imageName = $_POST["imageName"];
		
		if(authenticateUser($username, $password)){
			$userid = getUserId($username);
			
			$kj = round(($kcal / 0.2388), 1);
			
			$imagePath = "";
			if($image != ""){
				$imagePath = "https://syncreal.uni-bremen.de/barcode/img/".$imageName;
			
				$bin = base64_decode($image);
				$file = fopen('img/'.$imageName, 'wb');
				fwrite($file, $bin);
				fclose($file);			
			}
		
			$requestResult = insertNewProduct($userid, $barcode, $isDrink, $name, $fat, $satFat, $sugar, $salt, $kcal, $kj, $imagePath);
			
		} else {
			$requestResult = "Benutzername oder Passwort falsch!";
		}
		
	} else {		
		$user = "";
		$password = "";
		$barcode = "";
		$action = "";
			
		
		if(isset($_GET['user'])){
			$user = mysql_real_escape_string($_GET['user']);
		}
		if(isset($_GET['pw'])){
			$password = mysql_real_escape_string($_GET['pw']);
		}
		if(isset($_GET['barcode'])){
			$barcode = mysql_real_escape_string($_GET['barcode']);
		}
		if(isset($_GET['action'])){
			$action = mysql_real_escape_string($_GET['action']);
		}
		
		$requestResult = handleRequest($user, $password, $barcode, $action);
	}	
	
	echo json_encode($requestResult);
?>
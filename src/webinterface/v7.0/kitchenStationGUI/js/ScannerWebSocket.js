if (!window.WebSocket)
	alert("WebSocket not supported by this browser");

function $F() {
	return document.getElementById(arguments[0]).value;
}
	
var scannerWebsocket = {
	join : function() {
		var location = "ws://baall-mediapc.informatik.uni-bremen.de:16842/station/barcode/"
		
		this._ws = new WebSocket(location);
		this._ws.onopen = this._onopen;
		this._ws.onmessage = this._onmessage;
		this._ws.onclose = this._onclose;
		this._ws.onerror = this._onerror;
	},

	_onopen : function() {	
		document.getElementById('barcode').value = "0";
	},

	_onmessage : function(m) {
		if (m.data) {
			var c = m.data.indexOf(':');
			var from = m.data.substring(0, c).replace('<', '&lt;').replace('>', '&gt;');
			var text = m.data.substring(c + 1).replace('<', '&lt;').replace('>', '&gt;');
					
			document.getElementById('barcode').value = text;
			loadFromBarcoo(text);
		}
	},

	_onclose : function(m) {
		this._ws = null;
		document.getElementById('barcode').value = "";
	},

	_onerror : function(e) {
		alert(e);
	},
};
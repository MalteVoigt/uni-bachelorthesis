if (!window.WebSocket)
	alert("WebSocket not supported by this browser");

function $() {
	return document.getElementById(arguments[0]);
}

function $F() {
	return document.getElementById(arguments[0]).value;
}
	
var scaleWebsocket = {
	join : function() {
		var location = "ws://baall-mediapc.informatik.uni-bremen.de:16842/scale/weight/"
		
		this._ws = new WebSocket(location);
		this._ws.onopen = this._onopen;
		this._ws.onmessage = this._onmessage;
		this._ws.onclose = this._onclose;
		this._ws.onerror = this._onerror;
	},

	_onopen : function() {	
		$('weight').value = "0 g";
	},

	_onmessage : function(m) {
		if (m.data) {
			var c = m.data.indexOf(':');
			var from = m.data.substring(0, c).replace('<', '&lt;').replace(
					'>', '&gt;');
			var text = m.data.substring(c + 1).replace('<', '&lt;')
					.replace('>', '&gt;');
					
			$('weight').value = text;
		}
	},

	_onclose : function(m) {
		this._ws = null;
		$('weight').value = "";
	},

	_onerror : function(e) {
		alert(e);
	},
};
<?php	
	function askBarcoo($_barcode){
		$xml = new SimpleXMLElement(execApiRequest($_barcode));		
		
		$arr['name'] = $xml -> products -> product[0] -> title;
		$arr['picture'] = $xml -> products -> product[0] -> picture_low;
		
		$arr['is_drink'] = $xml -> products -> product[0] -> traffic_light -> is_drink;
		$arr['fat'] = $xml -> products -> product[0] -> traffic_light -> fat;
		$arr['sat_fat'] = $xml -> products -> product[0] -> traffic_light -> sat_fat;
		$arr['sugar'] = $xml -> products -> product[0] -> traffic_light -> sugar;
		$arr['salt'] = $xml -> products -> product[0] -> traffic_light -> salt;
		
		$arr['kcal'] = $xml -> products -> product[0] -> gda -> calorification;
		$arr['kj'] = $xml -> products -> product[0] -> nutritiondata -> kilojoule;
		
		return $arr;
	}
	
	function execApiRequest($_barcode){	
		$url = 'http://barcoo.com/api/get_product_complete?pi='.$_barcode.'&pins=ean&format=xml&source=dfki';
		
		$handle = fopen($url, "r");
		$content = '';
		
		while (!feof($handle)) {
			$content .= fread($handle, 8192);
		}
		fclose($handle);
		
		return $content;
	}
?>

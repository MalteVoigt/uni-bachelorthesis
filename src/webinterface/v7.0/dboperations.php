<?php	
	function authenticateUser($_username, $_password){
		$sql = mysql_query("SELECT * FROM user WHERE username = '$_username' AND password = '$_password'");
		
		if(mysql_num_rows($sql) == 1){
			return true;
		}
		return false;
	}
	

	function addProductToList($_barcode, $_userid){	
		$sql = mysql_query("SELECT * FROM product WHERE barcode = '$_barcode'");
		
		if(!is_resource($sql)){			
			$requestResult = "Fehler: Konnte Produkt nicht zur Einkaufsliste hinzufuegen: ".mysql_error();
		} else {
			if(mysql_num_rows($sql) == 1){ //wenn produkt in eigener db vorhanden
			
				$sql = mysql_query("SELECT count FROM list WHERE userid = '$_userid' AND barcode = '$_barcode'");
				
				if(mysql_num_rows($sql) == 1){ //falls das produkt schon auf der Liste steht, erh�he anzahl
					$row = mysql_fetch_array($sql);
			
					$count = $row['count'] + 1;
					
					if(!$sql = mysql_query("UPDATE list 
											SET count = '$count'
											WHERE userid = '$_userid' 
											  AND barcode = '$_barcode'")){
						$requestResult = "Fehler: Konnte Produkt nicht zur Einkaufsliste hinzufuegen: ".mysql_error();
					} else {
						$requestResult = "Produkt zur Einkaufsliste hinzugefuegt.";
					}
					
				} else { //sonst f�ge es neu hinzu
					if(!$sql = mysql_query("INSERT INTO list 
											VALUES ('$_userid', '$_barcode', '1')")){
						$requestResult = "Fehler: Konnte Produkt nicht zur Einkaufsliste hinzufuegen: ".mysql_error();
					} else {
						$requestResult = "Produkt zur Einkaufsliste hinzugefuegt.";
					}
				}
			} else { //wenn produkt nicht in eigener db vorhanden
				
				$arr = askBarcoo($_barcode);
				
				$name = $arr['name'];
				$isDrink = categorizeProduct($arr['is_drink']);
				
				//falls barcoo den barcode nicht kennt, kommt im namen der barcode vor
				if(strpos($name, $_barcode) === false){
					$sql = mysql_query("INSERT INTO product VALUES ('$_barcode', '$name', '', '')");
					
					$sql = mysql_query("INSERT INTO list VALUES ('$_userid', '$_barcode', '1')");
										
					$picture = $arr['picture'];
					$fat = $arr['fat'];
					$sat_fat = $arr['sat_fat'];
					$sugar = $arr['sugar'];
					$salt = $arr['salt'];					
					$kcal = $arr['kcal'];
					$kj = $arr['kj'];
										
					$sql = mysql_query("INSERT INTO details VALUES ('$_barcode', '$isDrink', '', '', 
																	'$fat', '$sat_fat', '$sugar', '$salt', 
																	'$kcal', '$kj', 
																	'$picture')");
					
					$requestResult = "Produkt zur Einkaufsliste hinzugefuegt.";
					
					insertOriginInformation($_barcode);
					
				} else {
					$requestResult = "Fehler: Unbekannter Barcode.";
				}
			}	
		}				
		return $requestResult;
	}

	
	function deleteProductFromList($_userid, $_barcode){
		$sql = mysql_query("DELETE FROM list 
							WHERE userid = '$_userid' 
							  AND barcode = '$_barcode'");
							  
		return "success";
	}
	
	
	function getShoppingList($_userid){	
		$requestResult = array();
	
		$sql = mysql_query("SELECT list.barcode, list.count, 
								   product.name, product.amount, 
								   unit.unit
							FROM list 
							JOIN product ON list.barcode = product.barcode AND userid = '$_userid'
							JOIN unit ON unit.id = product.unitid");
		
		if(is_resource($sql)){			
			while($row = mysql_fetch_array($sql)){
				$arr = array(
					"barcode" => $row['barcode'],
					"count" => $row['count'],
					"name" => $row['name'],
					"amount" => $row['amount'],
					"unit" => $row['unit']
				);				
				array_push($requestResult, $arr);
			}
		}
		return $requestResult;
	}
	
	
	function getProductDetails($_barcode){
		$requestResult = array();
		
		checkOriginInformation($_barcode);
		
		$sql = mysql_query("SELECT product.name, product.amount, 
								   details.is_drink, details.company, details.country, details.fat, details.sat_fat, details.sugar, details.salt, details.kcal, details.kj, details.image_path,
								   unit.unit
							FROM product 
							JOIN details ON details.barcode = product.barcode AND product.barcode = '$_barcode' 
							JOIN unit ON unit.id = product.unitid");
		
		if(is_resource($sql)){			
			while($row = mysql_fetch_array($sql)){
				$arr = array(				
					"name" => $row['name'],					
					"isDrink" => $row['is_drink'],
					"company" => $row['company'],
					"country" => $row['country'],
					"fat" => $row['fat'],
					"satFat" => $row['sat_fat'],
					"sugar" => $row['sugar'],
					"salt" => $row['salt'],
					"kcal" => $row['kcal'],
					"kj" => $row['kj'],
					"image_path" => $row['image_path']
				);	
				$requestResult = $arr;
			}
		}
		return $requestResult;
	}
	
	
	function insertNewProduct($userid, $barcode, $isDrink, $name, $fat, $satFat, $sugar, $salt, $kcal, $kj, $imagePath){
		$sql = mysql_query("INSERT INTO product VALUES ('$barcode', '$name', '', '')");
					
		$sql = mysql_query("INSERT INTO list VALUES ('$userid', '$barcode', '1')");
	
		$sql = mysql_query("INSERT INTO details VALUES ('$barcode', '$isDrink', '', '', 
														'$fat', '$satFat', '$sugar', '$salt', 
														'$kcal', '$kj', 
														'$imagePath')"); 
													
		insertOriginInformation($barcode);
		
		return "success";
	}
	
	
	/*
	 * ##### Helper Functions ######
	 */
		
	function getUserId($username){
		$sql = mysql_query("SELECT userid FROM user WHERE username = '$username'");
		$row = mysql_fetch_array($sql);
		
		if(mysql_num_rows($sql) == 1){
			return $row['userid'];
		}
		return -1;
	}
	
	function checkOriginInformation($barcode){
		$sql = mysql_query("SELECT company, country FROM details WHERE barcode = '$barcode'");
		$row = mysql_fetch_array($sql);
		
		if($row['company'] == "" || $row['country'] == ""){
			insertOriginInformation($barcode);
		}
	}
		
	function insertOriginInformation($barcode){
		$gepirData = getGepirContent($barcode);
		
		$company = trim(getCompany($gepirData));
		$country = trim(getCountry($gepirData));
		
		if($company != "GS1 Germany"){
			$sql = mysql_query("UPDATE details SET company = '$company', country = '$country' WHERE barcode = '$barcode'");
		}
	}
	
	function categorizeProduct($isDrink){
		if($isDrink == ""){ 
			return 2; 
		} else { 
			if($isDrink == "false"){
				return 0; 
			} else {
				return 1;
			}
		}
	}
?>






















 <?php
	function getCompany($relevant){
		$company = chopContent($relevant, '>', '<br />');
		
		return $company;
	}
	
	
	function getCountry($relevant){
		$country = chopContent($relevant, '<br />', '<td align="left"');
		$country = chopContent($country, '<br />', '<br />');
		$country = chopContent($country, '<br />', '</div>');
		
		return $country;
	}
	
 
	function chopContent($_content, $_stringFrom, $_stringTo){		
		$fromPos = strpos($_content, $_stringFrom) + strlen($_stringFrom);
		$toPos = strpos($_content, $_stringTo) - $fromPos;
		
		return (substr($_content, $fromPos, $toPos));
	}
 
 
	function getContent(){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://www.gepir.de/v31_client/gtin.aspx?Lang=de-DE");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, true);

		$data = array(
			'TabContainerGTIN:TabPanelGTIN:btnSubmitGTIN' => 'Suchen',
			'TabContainerGTIN:TabPanelGTIN:rblGTIN' => 'party',
			'TabContainerGTIN:TabPanelGTIN:txtRequestGTIN' => '5449000101549',
			'TabContainerGTIN_ClientState' => '{"ActiveTabIndex":0,"TabState":[true,false]}',
			'__EVENTVALIDATION' => '/wEWDgKv7vTwCgKr1s7pDwLb8/H6CQLX1L7/AwKE17zSCgKY4v+EDgKdyZOtCQLFy6SMDgLXoIt7AvzL05wNAuP/+aoMArb0pesMArGz4MkEAuu73LgOzJNz5ZQUbV7hg9mpp094wv+GzBU=',
			'__VIEWSTATE' => '/wEPDwUJMzUyOTg1OTYzD2QWAgIDD2QWBmYPDxYCHgdWaXNpYmxlaGRkAgEPZBYCAgMPZBYCAgMPPCsACgEADxYCHhJEZXN0aW5hdGlvblBhZ2VVcmwFDn4vRGVmYXVsdC5hc3B4ZGQCAw9kFgJmD2QWBgIBD2QWAmYPZBYCAgEPZBYCAgcPDxYCHgRUZXh0BQZTdWNoZW5kZAIDDw8WAh8AaGRkAgUPDxYCHwBnZBYQAgEPDxYCHwBnZGQCAg8PFgIfAGdkZAIDDw8WBh8CBRZHUzEgQmVsZ2l1bS9MdXhlbWJvdXJnHgtOYXZpZ2F0ZVVybAVKaHR0cDovL3d3dy5nczEub3JnLzEvY29udGFjdC93b3JsZHdpZGUucGhwP2FjdGlvbj1zaG93ZGV0YWlscyZjb3VudHJ5X2lkPTkfAGcWAh4Hb25DbGljawWaAXdpbmRvdy5vcGVuICh0aGlzLmhyZWYsICdwb3B1cHdpbmRvdycsJ3dpZHRoPTYwMCxoZWlnaHQ9NDUwLHNjcm9sbGJhcnM9MCxyZXNpemFibGU9MCx0b29sYmFyPTAsbG9jYXRpb249MCxkaXJlY3Rvcmllcz0wLHN0YXR1cz0wLG1lbnViYXI9MCcpO3JldHVybiBmYWxzZTtkAgQPFgQeC18hSXRlbUNvdW50AgEfAGcWAgIBD2QWBmYPFQwTNTQ0OTAwMDAwMDAwMjxiciAvPi5DT0NBLUNPTEEgU0VSVklDRVMgQUNDT1VOVFMgUEFZQUJMRSBEUFQuPGJyIC8+HENoYXVzc8OpZSBkZSBNb25zIDE0MjQ8YnIgLz4AAAAAAAAKMTA3MCZuYnNwOw9CUlVYRUxMRVM8YnIgLz4HQmVsZ2llbmQCAQ8WAh8FAgEWAgIBD2QWAmYPFQUAFlRlbDowMi81NTkuMjAuMDA8YnIgLz4WRmF4OjAyLzU1OS4yMC4wMTxiciAvPgAWd3d3LmNvY2Fjb2xhYmVsZ2l1bS5iZWQCAg8VBQoxMC4wMS4yMDEzDTU0NDkwMDA8YnIgLz4BMA01NDEwMDAwOTk5OTkzB0JlbGdpZW5kAgUPDxYEHwIFDTU0MTAwMDA5OTk5OTMfAGdkZAIGDw8WBB8CBQExHwBnZGQCBw8PFgQfAgUBMB8AZ2RkAggPDxYEHwIFEVN1Y2hlIGVyZm9sZ3JlaWNoHwBnZGQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgIFJUxvZ2luUGFuZWw6TG9naW5DdHJsOkxvZ2luSW1hZ2VCdXR0b24FEFRhYkNvbnRhaW5lckdUSU4FEFRhYkNvbnRhaW5lckdUSU4PD2RmZLlPHTzoN7srVtSyCcGMb/qlk8gA'
		);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$output = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		
		$relevant = chopContent($output, 'id="addressscroll"', 'id="Contact"');
		
		return $relevant;
	}	
?> 
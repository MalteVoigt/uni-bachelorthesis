<?	
	function getDetailedInformation($_barcode){	
		$content = getRawData($_barcode);
		$details = chopContent($content, "<!-- Weitere Produktdetails -->", "<!-- Liste der Links zu Stellen innerhalb der Seite -->");
	
		$arr = array(
			'productName' => getProductName($content),
			'subTitle' => getSubTitle($content),
			'amount' => getProductAmount($content),
			'barcode' => $_barcode,
			'image' => getProductImage($content),
			'producer' => getAbout($details, "Hersteller"),
			'origin' => getAbout($details, "Herkunft"),
			'energy' => getNutritionInfo($details, "Energie"),
			'protein' => getNutritionInfo($details, "Protein"),
			'carbohydrates' => getNutritionInfo($details, "Kohlenhydrate"),
			'fat' => getNutritionInfo($details, "Fett"),
			'natrium' => getNutritionInfo($details, "Natrium"),
			'dietaryFibre' => getNutritionInfo($details, "Ballaststoffe")
		);
		
		return $arr;
	}
	
	
	function getProductName($_content){
		$name = chopContent($_content, "h1Title", "</h1>");
		$name = substr($name, strpos($name, '>') + 1);	
		
		return replaceQuotes(trim($name));
	}
	
	
	function getSubTitle($_content){
		$name = chopContent($_content, "h2Subtitle", "</h2>");
		$name = substr($name, strpos($name, '>') + 1);
		
		return replaceQuotes(trim($name));
	}
	
	
	function getProductImage($_content) {		
		$path = "";
		$image = chopContent($_content, "<!-- Produktabbildung -->", "<!-- list user folder -->");
		
		if(substr_count($image, '<img src="') > 0){
			$image = substr($image, strpos($image, '<img src="') + strlen('<img src="'));
			$image = substr($image, 0, strpos($image, '"'));
			
			$path = "http://www.codecheck.info".$image;
		}
		
		return replaceQuotes($path);
	}
	
	
	function getProductAmount($_content){
		$amount = chopContent($_content, "<!-- Details zum Produkt -->", "<!-- Inhaltstoffe mit Links zu rechter Spalte -->");
		$amount = substr($amount, strpos($amount, 'Menge / ') + strlen('Menge / '));
		$amount = substr($amount, strpos($amount, '<br>') + strlen('<br>'));
		$amount = substr($amount, 0, strpos($amount, '<br>'));
		
		return replaceQuotes(trim($amount));
	}

	
	function getAbout($_content, $_about){
		$about = "";
		if(substr_count($_content, $_about) > 0){
			$about = substr($_content, strpos($_content, $_about));
			$about = chopContent($about, '<br>', "</div>");
			$about = str_replace('<br>', '', $about);
		}
		return replaceQuotes(trim($about));
	}
	
	
	function getNutritionInfo($_content, $_nutrition){
		$nutrition = "0";
		if(substr_count($_content, $_nutrition) > 0){
			$nutrition = substr($_content, strpos($_content, $_nutrition));
			$nutrition = chopContent($nutrition, '<span condition="false">', "</span>");
			$nutrition = str_replace('<span condition="false">', '', $nutrition);
			$nutrition = str_replace('g', '', $nutrition);
			$nutrition = str_replace(',', '.', $nutrition);
		}
		return replaceQuotes(trim($nutrition));
	}
	
	
	function chopContent($_content, $_stringFrom, $_stringTo){		
		$fromPos = strpos($_content, $_stringFrom);
		$toPos = strpos($_content, $_stringTo) - $fromPos;
		
		return (substr($_content, $fromPos, $toPos));
	}
	
	
	function replaceQuotes($_string){
		$tmp = str_replace("'", "", $_string);
		$tmp = str_replace("&nbsp;", "", $tmp);
		return $tmp;
	}
	
	
	function productExists($_content){		
		if(substr_count($_content, "Es wurde kein Produkt mit den Begriffen") > 0){
			return false;
		}
		return true;
	}
?>
<?php
	function handleRequest($_user, $_password, $_barcode, $_action){	
		switch($_action){
			case("authenticate");
				if(authenticateUser($_user, $_password)){
					$requestResult = "true";
				} else { 
					$requestResult = "Benutzername oder Passwort falsch!";
				}
				break;
				
				
				
			case("getProductDetails"):
				$requestResult = getProductDetails($_barcode);
				break;
				
				
				
			case("addProductToList"):	
				$requestResult = "Benutzername oder Passwort falsch!";
				
				if(authenticateUser($_user, $_password)){	
					$requestResult = addProductToList($_barcode, getUserId($_user));
				}
				break;
				
				
				
			case("deleteProductFromList"):
				$requestResult = "Benutzername oder Passwort falsch!";
				
				if(authenticateUser($_user, $_password)){					
					$requestResult = deleteProductFromList(getUserId($_user), $_barcode);
				}
				break;
				
				
				
			case("getShoppingList"):
				$requestResult = array();
			
				if(authenticateUser($_user, $_password)){
					$requestResult = getShoppingList(getUserId($_user));
				}
				break;
				
				
				
			default:
				$requestResult = "Fehler: Undefinierter Methodenaufruf.";
				break;
		}
		
		return $requestResult;
	}
?>
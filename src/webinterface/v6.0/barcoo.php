<?php	
	function askBarcoo($_barcode){
		$xml = new SimpleXMLElement(execApiRequest($_barcode));		
		
		$arr['name'] = $xml -> products -> product[0] -> title;
		$arr['picture'] = $xml -> products -> product[0] -> picture_low;
		
		//hole lebensmittelampel
		$arr['is_drink'] = $xml -> products -> product[0] -> gda -> is_drink;
		$arr['fat'] = $xml -> products -> product[0] -> gda -> fat;
		$arr['sat_fat'] = $xml -> products -> product[0] -> gda -> sat_fat;
		$arr['sugar'] = $xml -> products -> product[0] -> gda -> sugar;
		$arr['natrium'] = $xml -> products -> product[0] -> gda -> sodium;
		$arr['salt'] = $xml -> products -> product[0] -> gda -> salt;
		
		$arr['kcal'] = $xml -> products -> product[0] -> gda -> kilocalroies;
		$arr['kj'] = $xml -> products -> product[0] -> gda -> kilojoule;
		
		return $arr;
	}
	
	function execApiRequest($_barcode){	
		$url = 'http://barcoo.com/api/get_product_complete?pi='.$_barcode.'&pins=ean&format=xml&source=dfki';
		
		$handle = fopen($url, "r");
		$content = '';
		
		while (!feof($handle)) {
			$content .= fread($handle, 8192);
		}
		fclose($handle);
		
		return $content;
	}
?>

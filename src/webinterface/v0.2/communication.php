<?php
	function handleRequest($_action, $_barcode, $_userid){
		switch($_action){
			//Hauptinformationen zu einem Produkt
			case("getProduct"):				
				$tmpArr = getBasicInformation($_barcode);
				echo $tmpArr['title'];
				break;
			//Detailierte Informationen zu einem Produkt
			case("getProductDetails"):
				$product = getProduct($_barcode);
				break;
			//Produkt zur Einkaufsliste hinzuf�gen
			case("addProductToList"):
				$tmpArr = getBasicInformation($_barcode);
				createResponse($tmpArr['title']);
				//addProductToList($_userid, $_barcode, getProductName($product), getProductAmount($product));
				break;
			//Produkt von Einkaufsliste l�schen
			case("deleteProductFromList"):
				deleteProductFromList($_userid, $_barcode);
				break;
			//Produktanzahl erh�hen
			case("raiseProductCount"):
				raiseProductCount($_userid, $_barcode);
				break;
			//Produktanzahl verringern
			case("decreaseProductCount"):
				decreaseProductCount($_userid, $_barcode);
				break;
			//Einkaufsliste komplett zur�ckgeben
			case("getShoppingList"):
				echo getShoppingList($_userid);
				break;
			default:
				echo "Error: Undefined method call.";
				break;
		}
	}
	
	
	function getRawData($_barcode, $_source){			
		switch($_source){
			case("barcoo"):
				$url = 'http://barcoo.com/api/get_product_complete?pi='.$_barcode.'&pins=ean&format=xml&source=dfki';
				break;
			case("codecheck"):
				$url = 'http://www.codecheck.info/product.search?q='.$_barcode.'&OK=Suchen';
				break;
			default:
				break;
		}
		
		$handle = fopen($url, "r");
		$content = '';
		
		while (!feof($handle)) {
			$content .= fread($handle, 8192);
		}
		fclose($handle);
		
		return $content;
	}
	
	
	function createResponse($_message){
		echo $_message;
	}
?>
<?php	
	function getBasicInformation($_barcode){
		$xml = new SimpleXMLElement(getRawData($_barcode, "barcoo"));		
		
		$basicInformation['id'] = $xml -> products -> product[0] -> id;
		$basicInformation['title'] = $xml -> products -> product[0] -> title;
		$basicInformation['picture_low'] = $xml -> products -> product[0] -> picture_low;
		$basicInformation['producer'] = $xml -> products -> product[0] -> producer;

		return $basicInformation;
	}
?>

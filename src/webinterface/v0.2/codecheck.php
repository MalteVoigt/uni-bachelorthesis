<?
	function getDetailedInformation($_barcode){	
		$content = getRawData($_barcode, "codecheck");
	
		$detailedInformation['ingredients'] = getIngredients($content);
		$detailedInformation['energy'] = getEnergy($content);
		$detailedInformation['protein'] = getProtein($content);
		$detailedInformation['carbohydrates'] = getCarbohydrates($content);
		$detailedInformation['fat'] = getFat($content);
		$detailedInformation['natrium'] = getNatrium($content);
		$detailedInformation['dietaryFibre'] = getDietaryFibre($content);
		
		return $detailedInformation['energy'];
	}
	

	function chopContent($_content, $_stringFrom, $_stringTo){		
		$fromPos = strpos($_content, $_stringFrom);
		$toPos = strpos($_content, $_stringTo) - $fromPos;
		
		return (substr($_content, $fromPos, $toPos));
	}
	
	
	function getIngredients($_content){
		$ingredients = chopContent($_content, "<!-- Inhaltstoffe mit Links zu rechter Spalte -->", "<!-- Weitere Produktdetails -->");
		$ingredients = substr($ingredients, strpos($ingredients, '</span><br>') + strlen('</span><br>'));
		$ingredients = substr($ingredients, 0, strpos($ingredients, '</div>'));
		
		$part1 = substr($ingredients, 0, strpos($ingredients, '<'));
		$part2 = substr($ingredients, strpos($ingredients, '>')+1);
		
		$ingredients = $part1.$part2;
		
		return trim($ingredients);
	}
	
	
	function getEnergy($_content){
		$energy = chopContent($_content, "Energie / Brennwert", "Eiweiss / Proteine");
		$energy = substr($energy, strpos($energy, '<span condition="false">') + strlen('<span condition="false">'));
		$energy = substr($energy, 0, strpos($energy, '</span>'));
		
		return trim($energy);
	}
	
	
	function getProtein($_content){
		$protein = chopContent($_content, "Eiweiss / Proteine", "Kohlenhydrate");
		$protein = substr($protein, strpos($protein, '<span condition="false">') + strlen('<span condition="false">'));
		$protein = substr($protein, 0, strpos($protein, '</span>'));
		
		return trim($protein);
	}
	
	
	function getCarbohydrates($_content){
		$ch = chopContent($_content, "Kohlenhydrate", "Zucker");
		$ch = substr($ch, strpos($ch, '<span condition="false">') + strlen('<span condition="false">'));
		$ch = substr($ch, 0, strpos($ch, '</span>'));
		
		return trim($ch);
	}
	
	
	function getFat($_content){
		$fat = chopContent($_content, "Fett", "ges�ttigte Fetts�uren");
		$fat = substr($fat, strpos($fat, '<span condition="false">') + strlen('<span condition="false">'));
		$fat = substr($fat, 0, strpos($fat, '</span>'));
		
		return trim($fat);
	}
	
	
	function getNatrium($_content){
		$natrium = chopContent($_content, "Natrium / Salz", "Ballaststoffe / Nahrungsfasern");
		$natrium = substr($natrium, strpos($natrium, '<span condition="false">') + strlen('<span condition="false">'));
		$natrium = substr($natrium, 0, strpos($natrium, '</span>'));
		
		return trim($natrium);
	}
	
	
	function getDietaryFibre($_content){
		$df = chopContent($_content, "Ballaststoffe / Nahrungsfasern", "Letzte �nderung");
		$df = substr($df, strpos($df, '<span condition="false">') + strlen('<span condition="false">'));
		$df = substr($df, 0, strpos($df, '</span>'));
		
		return trim($df);
	}
?>
<?php	
	function addProductToList($_userid, $_barcode, $_productName, $_productAmount){
		$result = "";
		
		$sql = mysql_query("SELECT id 
							FROM list 
							WHERE userid = '$_userid' 
							  AND barcode = '$_barcode'");
		
		if(!is_resource($sql)){			
			$result = "Fehler: Konnte Produkt nicht zur Einkaufsliste hinzufuegen: ".mysql_error();
		} else {
			if(mysql_num_rows($sql) > 0){
				$result = raiseProductCount($_userid, $_barcode, $_productName);
			} else {
				if(!$sql = mysql_query("INSERT INTO list 
										VALUES ('', '$_userid', '$_barcode', '$_productName', '$_productAmount', '1')")){
					$result = "Fehler: Konnte Produkt nicht zur Einkaufsliste hinzufuegen: ".mysql_error();
				} else {
					$result = "Produkt '".$_productName."' zur Einkaufsliste hinzugefuegt.";
				}
			}	
		}		
		return $result;
	}
	
	
	function raiseProductCount($_userid, $_barcode, $_productName){
		$sql = mysql_query("SELECT count 
							FROM list 
							WHERE userid = '$_userid' 
							  AND barcode = '$_barcode'");
		
		if(!is_resource($sql)){	
			$result = "Fehler: Konnte Produkt nicht zur Einkaufsliste hinzufuegen: ".mysql_error();
		} else {
			$row = mysql_fetch_array($sql);
			
			$count = $row['count'] + 1;
			
			if(!$sql = mysql_query("UPDATE list 
								    SET count = '$count'
									WHERE userid = '$_userid' 
									  AND barcode = '$_barcode'")){
				$result = "Fehler: Konnte Produkt nicht zur Einkaufsliste hinzufuegen: ".mysql_error();
			} else {
				$result = "Produkt '".$_productName."' zur Einkaufsliste hinzugefuegt.";
			}
		}		
		return $result;
	}
	
	
	function getShoppingList($_userid){
		$result = array();
		$state = "";
	
		$sql = mysql_query("SELECT id, barcode, product_name, amount, count
							FROM list 
							WHERE userid = '$_userid'");
		
		if(!is_resource($sql)){
			$state = "Fehler: Konnte Einkaufsliste nicht aus Datenbank abfragen: ".mysql_error();
		} else {	
			$i = 1;
		
			while($row = mysql_fetch_array($sql)){
				$arr = array(
					"id" => $row['id'],
					"barcode" => $row['barcode'],
					"product_name" => $row['product_name'],
					"amount" => $row['amount'],
					"count" => $row['count']
				);
				
				$result[$i] = $arr;
				$i++;
			}
		}	
		$result[0] = $state;
		
		return $result;
	}
	
	/*
	function deleteProductFromList($_userid, $_ean){
		$sql = mysql_query("DELETE FROM list 
							WHERE userid = '$_userid' 
							  AND ean = '$_ean'");
	}
	
	
	function decreaseProductCount($_userid, $_ean){
		$sql = mysql_query("SELECT count 
							FROM list 
							WHERE userid = '$_userid' 
							  AND ean = '$_ean'");
		$row = mysql_fetch_array($sql);
		
		$count = $row['count'] - 1;
		if($count > 0){
			$sql = mysql_query("UPDATE list 
								SET count = '$count'
								WHERE userid = '$_userid' 
								  AND ean = '$_ean'");			
		} else {
			deleteProductFromList($_userid, $_ean);
		}		
	}
	*/
?>
<?
	function chopContent($_content, $_stringFrom, $_stringTo){		
		$fromPos = strpos($_content, $_stringFrom);
		$toPos = strpos($_content, $_stringTo) - $fromPos;
		
		return (substr($_content, $fromPos, $toPos));
	}
	
	
	function replaceQuotes($_string){
		return str_replace("'", "", $_string);
	}
	
	
	function productExists($_content){		
		if(substr_count($_content, "Es wurde kein Produkt mit den Begriffen") > 0){
			return false;
		}
		return true;
	}
	
	
	function getDetailedInformation($_barcode){	
		$content = getRawData($_barcode);
	
		$detailedInformation['name'] = getProductName($content);
		$detailedInformation['image'] = getProductImage($content);
		$detailedInformation['amount'] = getProductAmount($content);
		$detailedInformation['producer'] = getProducer($content);
		$detailedInformation['origin'] = getOrigin($content);
		$detailedInformation['ingredients'] = getIngredients($content);
		$detailedInformation['energy'] = getEnergy($content);
		$detailedInformation['protein'] = getProtein($content);
		$detailedInformation['carbohydrates'] = getCarbohydrates($content);
		$detailedInformation['fat'] = getFat($content);
		$detailedInformation['natrium'] = getNatrium($content);
		$detailedInformation['dietaryFibre'] = getDietaryFibre($content);
		
		return $detailedInformation;
	}
	
	
	function getProductName($_content, $_barcode){
		$name = chopContent($_content, "<title>", "</title>");
		$name = substr($name, strpos($name, '<title>') + strlen('<title>'));
		$name = substr($name, 0, strpos($name, '- '.$_barcode));
		
		return replaceQuotes(trim($name));
	}
	
	
	function getProductImage($_content) {		
		$image = chopContent($_content, "<!-- Produktabbildung -->", "<!-- list user folder -->");		
		$image = substr($image, strpos($image, '<img src="') + strlen('<img src="'));
		$image = substr($image, 0, strpos($image, '"'));
		
		$path = "http://www.codecheck.info".$image;
		
		return replaceQuotes($path);
	}
	
	
	function getProductAmount($_content){
		$amount = chopContent($_content, "<!-- Details zum Produkt -->", "<!-- Inhaltstoffe mit Links zu rechter Spalte -->");
		$amount = substr($amount, strpos($amount, 'Menge / ') + strlen('Menge / '));
		$amount = substr($amount, strpos($amount, '<br>') + strlen('<br>'));
		$amount = substr($amount, 0, strpos($amount, '<br>'));
		
		return replaceQuotes(trim($amount));
	}

	
	function getProducer($_content){
		$producer = chopContent($_content, "<!-- Weitere Produktdetails -->", "<!-- Liste der Links zu Stellen innerhalb der Seite -->");
		$producer = substr($producer, strpos($producer, '</span><br>') + strlen('</span><br>'));
		$producer = substr($producer, 0, strpos($producer, '<br>'));
		
		return replaceQuotes(trim($producer));
	}
	
	
	function getOrigin($_content){
		$origin = chopContent($_content, "<!-- Weitere Produktdetails -->", "<!-- Liste der Links zu Stellen innerhalb der Seite -->");
		$origin = substr($origin, strpos($origin, 'Herkunft') + strlen('Herkunft'));
		$origin = substr($origin, strpos($origin, '</span><br>') + strlen('</span><br>'));
		$origin = substr($origin, 0, strpos($origin, '<br>'));
		
		return replaceQuotes(trim($origin));
	}
	
	
	function getIngredients($_content){
		$ingredients = chopContent($_content, "<!-- Inhaltstoffe mit Links zu rechter Spalte -->", "<!-- Weitere Produktdetails -->");
		$ingredients = substr($ingredients, strpos($ingredients, '</span><br>') + strlen('</span><br>'));
		$ingredients = substr($ingredients, 0, strpos($ingredients, '</div>'));
		
		$part1 = substr($ingredients, 0, strpos($ingredients, '<'));
		$part2 = substr($ingredients, strpos($ingredients, '>')+1);
		
		$ingredients = $part1.$part2;
		
		return replaceQuotes(trim($ingredients));
	}
	
	
	function getEnergy($_content){
		$energy = chopContent($_content, "Energie / Brennwert", "Eiweiss / Proteine");
		$energy = substr($energy, strpos($energy, '<span condition="false">') + strlen('<span condition="false">'));
		$energy = substr($energy, 0, strpos($energy, '</span>'));
		
		return replaceQuotes(trim($energy));
	}
	
	
	function getProtein($_content){
		$protein = chopContent($_content, "Eiweiss / Proteine", "Kohlenhydrate");
		$protein = substr($protein, strpos($protein, '<span condition="false">') + strlen('<span condition="false">'));
		$protein = substr($protein, 0, strpos($protein, '</span>'));
		
		return replaceQuotes(trim($protein));
	}
	
	
	function getCarbohydrates($_content){
		$ch = chopContent($_content, "Kohlenhydrate", "Zucker");
		$ch = substr($ch, strpos($ch, '<span condition="false">') + strlen('<span condition="false">'));
		$ch = substr($ch, 0, strpos($ch, '</span>'));
		
		return replaceQuotes(trim($ch));
	}
	
	
	function getFat($_content){
		$fat = chopContent($_content, "Fett", "ges�ttigte Fetts�uren");
		$fat = substr($fat, strpos($fat, '<span condition="false">') + strlen('<span condition="false">'));
		$fat = substr($fat, 0, strpos($fat, '</span>'));
		
		return replaceQuotes(trim($fat));
	}
	
	
	function getNatrium($_content){
		$natrium = chopContent($_content, "Natrium / Salz", "Ballaststoffe / Nahrungsfasern");
		$natrium = substr($natrium, strpos($natrium, '<span condition="false">') + strlen('<span condition="false">'));
		$natrium = substr($natrium, 0, strpos($natrium, '</span>'));
		
		return replaceQuotes(trim($natrium));
	}
	
	
	function getDietaryFibre($_content){
		$df = chopContent($_content, "Ballaststoffe / Nahrungsfasern", "Letzte �nderung");
		$df = substr($df, strpos($df, '<span condition="false">') + strlen('<span condition="false">'));
		$df = substr($df, 0, strpos($df, '</span>'));
		
		return replaceQuotes(trim($df));
	}
?>
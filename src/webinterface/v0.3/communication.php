<?php
	function handleRequest($_action, $_barcode, $_userid){
		$requestResult = "";
	
		switch($_action){
			case("getProduct"):	
				break;
				
			case("getProductDetails"):
				break;
				
			case("addProductToList"):
				$content = getRawData($_barcode);
				
				if(productExists($content)){
					$productName = getProductName($content, $_barcode);
					$productAmount = getProductAmount($content);
					$databaseActionResult = addProductToList($_userid, $_barcode, $productName, $productAmount);
					$requestResult = $databaseActionResult;
				} else {
					$requestResult = "Fehler: Produkt nicht gefunden!";
				}
				break;
				
			case("deleteProductFromList"):
				break;
				
			case("raiseProductCount"):
				break;
				
			case("decreaseProductCount"):
				break;
				
			case("getShoppingList"):
				break;
				
			default:
				$requestResult = "Fehler: Undefinierter Methodenaufruf.";
				break;
		}
		
		return $requestResult;
	}
	
	
	function getRawData($_barcode){
		$url = 'http://www.codecheck.info/product.search?q='.$_barcode.'&OK=Suchen';
		
		$handle = fopen($url, "r");
		$content = '';
		
		while (!feof($handle)) {
			$content .= fread($handle, 8192);
		}
		fclose($handle);
		
		return $content;
	}
	
	
	function createResponse($_response){
		echo "<response>".$_response."</response>";
	}
?>
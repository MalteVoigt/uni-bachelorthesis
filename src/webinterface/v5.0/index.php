<?php
	include "config.php";
	include "communication.php";
	include "codecheck.php";
	include "dboperations.php";
		
	mysql_set_charset("utf8", $db_link);
	
	$user = "";
	$pw = "";
	$barcode = "";
	$action = "";
		
	
	if(isset($_GET['user'])){
		$user = mysql_real_escape_string($_GET['user']);
	}
	if(isset($_GET['pw'])){
		$password = mysql_real_escape_string($_GET['pw']);
	}
	if(isset($_GET['barcode'])){
		$barcode = mysql_real_escape_string($_GET['barcode']);
	}
	if(isset($_GET['action'])){
		$action = mysql_real_escape_string($_GET['action']);
	}
	
	
	$requestResult = handleRequest($user, $password, $barcode, $action);
	
	echo json_encode($requestResult);
?>
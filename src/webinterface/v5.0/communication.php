<?php
	function handleRequest($_user, $_password, $_barcode, $_action){	
		switch($_action){
			case("authenticate");
				if(authenticateUser($_user, $_password)){
					$requestResult = "true";
				} else { 
					$requestResult = "Benutzername oder Passwort falsch!";
				}
				break;
				
				
				
			case("getProductDetails"):
				$requestResult = getProductDetails($_barcode);
				break;
				
				
				
			case("addProductToList"):	
				$requestResult = "Benutzername oder Passwort falsch!";
				
				//nutzer authentifizieren
				//addProductToList()
				//		r�ckgabe = Fehler: Unbekannter Barcode.
				//			- NEIN: fertig!
				//			- JA: getRawData()
				//					productExists()
				//						- NEIN: Fehler
				//						- JA: f�ge produkt in db ein (confirmed = 0)
					
				if(authenticateUser($_user, $_password)){		
					$content = getRawData($_barcode);
					
					if(productExists($content)){
						$requestResult = addProductToList(getUserId($_user), $_barcode);
					} else {
						$requestResult = "Fehler: Produkt nicht gefunden!";
					}
				}
				break;
				
				
				
			case("addProductFromMobile"):	
				$requestResult = addProductFromMobile($_barcode, $_user);
				break;
				
				
				
			case("deleteProductFromList"):
				$requestResult = "Benutzername oder Passwort falsch!";
				
				if(authenticateUser($_user, $_password)){					
					$requestResult = deleteProductFromList(getUserId($_user), $_barcode);
				}
				break;
				
				
				
			case("getShoppingList"):
				$requestResult = array();
			
				if(authenticateUser($_user, $_password)){
					$requestResult = getShoppingList(getUserId($_user));
				}
				break;
				
				
				
			default:
				$requestResult = "Fehler: Undefinierter Methodenaufruf.";
				break;
		}
		
		return $requestResult;
	}
	
	
	function getRawData($_barcode){
		$url = 'http://www.codecheck.info/product.search?q='.$_barcode.'&OK=Suchen';
		
		$handle = fopen($url, "r");
		$content = '';
		
		while (!feof($handle)) {
			$content .= fread($handle, 8192);
		}
		fclose($handle);
		
		return $content;
	}
?>
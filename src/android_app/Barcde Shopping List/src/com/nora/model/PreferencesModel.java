package com.nora.model;

import com.nora.R;
import com.nora.util.Preferences;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;

public class PreferencesModel extends PreferenceActivity {
	
	protected Preferences preferences;
	
	private ListPreference updateIntervalPref;
	
	private CheckBoxPreference saveUserdataPref;
	
	private CheckBoxPreference automaticLoginPref;
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.preferences);
		
		preferences = new Preferences(this);
		
		updateIntervalPref = (ListPreference) findPreference("updateIntervals");
		updateIntervalPref.setValue(convertIntervalToKey(preferences.getUpdatePeriod()));
		updateIntervalPref.setOnPreferenceChangeListener(changeUpdateIntervalListener());
		
		saveUserdataPref = (CheckBoxPreference) findPreference("save_userdata");
		saveUserdataPref.setChecked(preferences.getUserdataState());
		saveUserdataPref.setOnPreferenceClickListener(changeSaveUserdataListener());
		
		automaticLoginPref = (CheckBoxPreference) findPreference("automatic_login");
		automaticLoginPref.setChecked(preferences.getLoginState());
		automaticLoginPref.setOnPreferenceClickListener(changeAutomaticLoginListener());
	}
	
	
	private OnPreferenceChangeListener changeUpdateIntervalListener(){
		return new OnPreferenceChangeListener(){
			public boolean onPreferenceChange(Preference _preference, Object _value) {
				preferences.setUpdatePeriod(convertKeyToInterval(_value.toString()));
				return false;
			}
		};
	}
	
	
	private OnPreferenceClickListener changeSaveUserdataListener(){
		return new OnPreferenceClickListener(){
			public boolean onPreferenceClick(Preference _value) {
				if(preferences.getUserdataState()){
					preferences.setUserdataState(false);
					if(preferences.getLoginState()){
						preferences.setLoginState(false);
						automaticLoginPref.setChecked(false);
					}
				} else {
					preferences.setUserdataState(true);
				}
				return false;
			}
		};
	}
	
	
	private OnPreferenceClickListener changeAutomaticLoginListener(){
		return new OnPreferenceClickListener(){
			public boolean onPreferenceClick(Preference preference) {
				if(preferences.getLoginState()){
					preferences.setLoginState(false);
				} else {
					preferences.setLoginState(true);
					preferences.setUserdataState(true);
					saveUserdataPref.setChecked(true);
				}
				return false;
			}
		};
	}
	
	
	private long convertKeyToInterval(String _key){
		long interval = 1800000;
		
		if(_key.equalsIgnoreCase("5 Minuten")){
			interval = 300000;
		}
		else if(_key.equalsIgnoreCase("10 Minuten")){
			interval = 600000;
		}
		else if(_key.equalsIgnoreCase("15 Minuten")){
			interval = 900000;
		}
		else if(_key.equalsIgnoreCase("30 Minuten")){
			interval = 1800000;
		}
		else if(_key.equalsIgnoreCase("1 Stunde")){
			interval = 3600000;
		}
		else if(_key.equalsIgnoreCase("2 Stunde")){
			interval = 7200000;
		}
		else if(_key.equalsIgnoreCase("4 Stunden")){
			interval = 14400000;
		}
		else if(_key.equalsIgnoreCase("8 Stunden")){
			interval = 28800000;
		}
		else if(_key.equalsIgnoreCase("16 Stunden")){
			interval = 57600000;
		}
		else if(_key.equalsIgnoreCase("24 Stunden")){
			interval = 115200000;
		}
		
		return interval;
	}
	
	
	private String convertIntervalToKey(long _interval){
		String key = "30 Minuten";
		
		if(_interval == 300000){
			key = "5 Minuten";
		}
		else if(_interval == 600000){
			key = "10 Minuten";
		}
		else if(_interval == 900000){
			key = "15 Minuten";
		}
		else if(_interval == 1800000){
			key = "30 Minuten";
		}
		else if(_interval == 3600000){
			key = "1 Stunde";
		}
		else if(_interval == 7200000){
			key = "2 Stunden";
		}
		else if(_interval == 14400000){
			key = "4 Stunden";
		}
		else if(_interval == 28800000){
			key = "8 Stunden";
		}
		else if(_interval == 57600000){
			key = "16 Stunden";
		}
		else if(_interval == 115200000){
			key = "24 Stunden";
		}
		
		return key;
	}

}

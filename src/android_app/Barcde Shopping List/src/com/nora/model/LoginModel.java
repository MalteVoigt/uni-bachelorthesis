package com.nora.model;

import com.nora.R;
import com.nora.network.ApiRequest;
import com.nora.util.Preferences;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginModel extends Activity implements OnClickListener, OnFocusChangeListener {
	
	private EditText et_username;	
	private EditText et_password;
	
	private TextView tv_error;
	
	private String username;	
	private String password;	
	
	private Button btn_login;
	
	protected Preferences preferences;
	

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.login);
           
        et_username = (EditText) findViewById(R.id.et_username);
        et_username.setOnFocusChangeListener(this);
        
        et_password = (EditText) findViewById(R.id.et_password);
        et_password.setOnFocusChangeListener(this);
        
        tv_error = (TextView) findViewById(R.id.tv_error);
        
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);

        tv_error.setFocusable(true);
        tv_error.setFocusableInTouchMode(true);
        tv_error.requestFocus();
        
        preferences = new Preferences(this);
        
        findViewById(R.id.mainLayout).requestFocus();
        
        if(preferences.getLoginState()){
        	username = preferences.getLastActiveUser();
        	password = preferences.getLastActiveUserPassword();
        	
        	if(username != "" && password != ""){
        		onSuccess();
        	}
        }
        
        username = "malte";
        password = "test";
        onSuccess();
    }

   
	public void login(){
		if(isConnected()){
			username = et_username.getText().toString();
			password = et_password.getText().toString();
			
			if(!username.isEmpty() && !password.isEmpty() && 
			   !username.equalsIgnoreCase("Benutzername") && !password.equalsIgnoreCase("Passwort")){
				new ApiRequest(this, "Bitte warten...", "authenticate", "", username, password).execute();
			} else {
				onFail("Bitte Benutzername und Passwort angeben!");
			}
		} else {
			onFail("Keine Internetverbindung!");
		}
    }
	
	
	public void onSuccess(){
		Intent intent = new Intent(this, ShoppingListModel.class);
		intent.putExtra("username", username);
		intent.putExtra("password", password);
		startActivity(intent);
	}
	
	
	public void onFail(String _message){
		tv_error.setVisibility(View.VISIBLE);
		tv_error.setText(_message);
	}
    
    
    private boolean isConnected(){
    	ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    	
	    NetworkInfo networkInfo = cm.getActiveNetworkInfo();
	    
	    if (networkInfo != null && networkInfo.isConnected()) {
	    	return true;
	    }
	    return false;
    }


	public void onClick(View view) {		
		if(view == btn_login){
			login();
		}
	}


	public void onFocusChange(View view, boolean hasFocus) {
		if(view == et_username){
			if(hasFocus && et_username.getText().toString().equalsIgnoreCase("Benutzername")){
				et_username.setText("");
				et_username.setTextColor(Color.BLACK);
			} else {
				if(!hasFocus && et_username.getText().toString().equalsIgnoreCase("")){
					et_username.setText("Benutzername");
					et_username.setTextColor(Color.parseColor("#C0C0C0"));
				}
			}
		}

		if(view == et_password){
			if(hasFocus && et_password.getText().toString().equalsIgnoreCase("Passwort")){
				et_password.setText("");
				et_password.setTextColor(Color.BLACK);
			} else {
				if(!hasFocus && et_password.getText().toString().equalsIgnoreCase("")){
					et_password.setText("Passwort");
					et_password.setTextColor(Color.parseColor("#C0C0C0"));
				}
			}
		}
	}
    
}

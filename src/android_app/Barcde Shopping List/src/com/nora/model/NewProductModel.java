package com.nora.model;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nora.R;
import com.nora.network.InsertProductRequest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class NewProductModel extends Activity implements OnClickListener {
	
	private static final int CAMERA_REQUEST = 1337;
	
	private static final int IMAGE = 42;
	
	private Bitmap image;
	
	private String username;
	private String password;
	private String barcode;

	private EditText et_productName;	
	private EditText et_fat;	
	private EditText et_satFat;	
	private EditText et_sugar;	
	private EditText et_salt;	
	private EditText et_energy;	
	
	private ImageView iv_productImage;
	
	private ImageButton btn_save;
	
	private TextView tv_fatPortion;
	private TextView tv_satFatPortion;
	private TextView tv_sugarPortion;
	private TextView tv_saltPortion;
	private TextView tv_energyPortion;
	
	private RadioGroup rg_kindOfProduct;
	
	private RadioButton rb_food;
	private RadioButton rb_drink;
	private RadioButton rb_other;
	
	private LinearLayout ll_nutrition;
	
	

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.new_product);	       
        
        Bundle extras = getIntent().getExtras();        
        if(extras != null){
        	barcode = extras.getString("barcode");
        	username = extras.getString("username");
        	password = extras.getString("password");
        }

        et_productName = (EditText) findViewById(R.id.et_productName);
        et_fat = (EditText) findViewById(R.id.et_fat);
        et_satFat = (EditText) findViewById(R.id.et_satFat);
        et_sugar = (EditText) findViewById(R.id.et_sugar);
        et_salt = (EditText) findViewById(R.id.et_salt);
        et_energy = (EditText) findViewById(R.id.et_energy);

        tv_fatPortion = (TextView) findViewById(R.id.tv_fatPortion);
        tv_satFatPortion = (TextView) findViewById(R.id.tv_satFatPortion);
        tv_sugarPortion = (TextView) findViewById(R.id.tv_sugarPortion);
        tv_saltPortion = (TextView) findViewById(R.id.tv_saltPortion);
        tv_energyPortion = (TextView) findViewById(R.id.tv_energyPortion);
        
        rg_kindOfProduct = (RadioGroup) findViewById(R.id.rg_kindOfProduct);
        rb_food = (RadioButton) findViewById(R.id.rb_food);
        rb_food.setOnClickListener(this);
        rb_drink = (RadioButton) findViewById(R.id.rb_drink);
        rb_drink.setOnClickListener(this);
        rb_other = (RadioButton) findViewById(R.id.rb_other);
        rb_other.setOnClickListener(this);

        iv_productImage = (ImageView) findViewById(R.id.iv_productImage);
        iv_productImage.setOnClickListener(this);
                
        btn_save = (ImageButton) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);
        
        ll_nutrition = (LinearLayout) findViewById(R.id.ll_nutrition);
    }


	public void onClick(View v) {
		if(v == btn_save){
			validate();
		}
		else if(v == rb_food){
			ll_nutrition.setVisibility(View.VISIBLE);
			tv_fatPortion.setText("g/100g");
			tv_satFatPortion.setText("g/100g");
			tv_sugarPortion.setText("g/100g");
			tv_saltPortion.setText("g/100g");
			tv_energyPortion.setText("kcal/100g");
		}
		else if(v == rb_drink){
			ll_nutrition.setVisibility(View.VISIBLE);
			tv_fatPortion.setText("g/100ml");
			tv_satFatPortion.setText("g/100ml");
			tv_sugarPortion.setText("g/100ml");
			tv_saltPortion.setText("g/100ml");
			tv_energyPortion.setText("kcal/100ml");
		}
		else if(v == rb_other){
			ll_nutrition.setVisibility(View.INVISIBLE);
		}
		else if(v == iv_productImage){
			Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
			startActivityForResult(cameraIntent, CAMERA_REQUEST); 
		}
	}
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
			image = (Bitmap) data.getExtras().get("data"); 
			iv_productImage.setImageBitmap(image);
		}
	}
	
	
	private void validate(){
		RadioButton tmp = (RadioButton) findViewById(rg_kindOfProduct.getCheckedRadioButtonId());

		int isDrink;
		if(tmp == rb_food){
			isDrink = 0;
		} else {
			if(tmp == rb_drink){
				isDrink = 1;
			} else {
				isDrink = 2;
			}
		}
		
		String productName = et_productName.getText().toString();
		productName = productName.replace("�", "ae");
		productName = productName.replace("�", "ue");
		productName = productName.replace("�", "oe");
		productName = productName.replace("�", "Ae");
		productName = productName.replace("�", "Ue");
		productName = productName.replace("�", "Oe");
		productName = productName.replace("�", "ss");
		
		if(!productName.equalsIgnoreCase("Produktname")){
			double fat = Double.parseDouble(et_fat.getText().toString());
			double satFat = Double.parseDouble(et_satFat.getText().toString());
			double sugar = Double.parseDouble(et_sugar.getText().toString());
			double salt = Double.parseDouble(et_salt.getText().toString());
			double energy = Double.parseDouble(et_energy.getText().toString());
			
			String base64 = "";
			if(image != null){
				base64 = decodeImage(image);
			}
			
			new InsertProductRequest(this, username, password, barcode, isDrink, productName, fat, satFat, sugar, salt, energy, base64, getFileName()).execute();
			
		} else {
			Toast.makeText(this, "Bitte geben Sie einen Produktnamen an!", Toast.LENGTH_LONG).show();
		}
	}
	
	
	private String getFileName(){
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    double rnd = Math.random();
	    
	    String fileName = "IMG_" + timeStamp + String.valueOf(rnd) + ".jpg";
	    
	    return fileName;
	}
	
	
	private String decodeImage(Bitmap image){
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		
		image.compress(Bitmap.CompressFormat.JPEG, 90, stream);
	    byte[] base64 = stream.toByteArray();

	    return Base64.encodeToString(base64, Base64.DEFAULT);
	}
}

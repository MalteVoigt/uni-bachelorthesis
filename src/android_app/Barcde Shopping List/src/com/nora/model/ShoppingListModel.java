package com.nora.model;

import java.util.ArrayList;
import java.util.List;

import com.nora.R;
import com.nora.database.DatabaseHandler;
import com.nora.network.ApiRequest;
import com.nora.util.Entry;
import com.nora.util.EntryArrayAdapter;
import com.nora.util.Preferences;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ShoppingListModel extends Activity {
	
	public static Activity context;
	
	private ListView shoppingListView;
	
	private View shoppingListHeader;
	
	private ImageView updateListImage;
	
	private ImageView scanBarcodeImage;
	
	private TextView networkStateTextView;
	
	private ArrayList<Entry> shoppingList;
	
	private ArrayAdapter<Entry> arrayAdapter;
	
	private String username;
	
	private String password;
	
	private Handler automaticUpdateHandler;	
	
	protected Preferences preferences;
	
	private DatabaseHandler database;
	
	private long updateInterval;
	
	private String lastManuallyScannedBarcode;
	
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.shopping_list);
        
        context = this;
        
        Bundle extras = getIntent().getExtras();        
        if(extras != null){
        	username = extras.getString("username");
        	password = extras.getString("password");
        }

        shoppingListHeader = getLayoutInflater().inflate(R.layout.list_header, null);
        
        shoppingListView = (ListView) findViewById(R.id.shoppingList);        
        shoppingListView.addHeaderView(shoppingListHeader);        
        shoppingListView.setOnItemClickListener(showDetailsListener());
        
        updateListImage = (ImageView) findViewById(R.id.reloadImage);
        updateListImage.setOnClickListener(updateListListener());
        
        scanBarcodeImage = (ImageView) findViewById(R.id.scanBarcodeImage);
        scanBarcodeImage.setOnClickListener(scanBarcodeListener());
        
        networkStateTextView = (TextView) findViewById(R.id.networkState);
        
        shoppingList = new ArrayList<Entry>();    
        
        arrayAdapter = new EntryArrayAdapter(this, shoppingList); 
        
        automaticUpdateHandler = new Handler();
        
        preferences = new Preferences(this);
        
        database = new DatabaseHandler(this);
           	
        
        registerConnectiviyListener();
        downloadShoppingList();
        startAutomaticUpdateRoutine();        
    }
    
    
    @Override
    protected void onResume(){  
    	super.onResume();
    	if(updateInterval != preferences.getUpdatePeriod()){
    		updateInterval = preferences.getUpdatePeriod();
    		startAutomaticUpdateRoutine();
    	}
    	if(preferences.getLoginState()){
    		preferences.setLastActiveUser(username);
    		preferences.setLastActiveUserPassword(password);
    	} else {
    		preferences.setLastActiveUser("");
    		preferences.setLastActiveUserPassword("");
    	}
    }
        
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    	stopAutomaticUpdateRoutine();
    }
    
   
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.shopping_list, menu);
        return true;
    }
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    		case R.id.appSettings:
    			Intent settings = new Intent(ShoppingListModel.this, PreferencesModel.class);
    			this.startActivity(settings);
    			return true;
    		case R.id.updateList:
    			downloadShoppingList();
    			return true;
    		case R.id.logout:
    			this.finish();
    			return true;
    		case R.id.scanBarcode:
    			try{
	                Intent scan = new Intent("com.google.zxing.client.android.SCAN");
	                scan.putExtra("SCAN_MODE", "PRODUCT_MODE");
	                startActivityForResult(scan, 0);
            	} catch (ActivityNotFoundException e){
            		Toast.makeText(getApplicationContext(), "Zxing Barcode Scanner App nicht installiert. Scannen nicht m�glich.", 10000).show();
            	}
                return true;
    		default:
    			return super.onOptionsItemSelected(item);
    	}
    }
    
    
    /**
     * Erzeugt einen neuen ApiRequest welcher die Einkaufsliste des Benutzers
     * vom Server herunterl�dt. Ist keine Internetverbindung vorhanden,
     * wird der letzte Stand der Einkaufsliste aus der lokalen Datenbank
     * geladen.
     */
    public void downloadShoppingList(){
    	if(isConnected()){
    		new ApiRequest(this, "Lade Einkaufsliste...", "getShoppingList", "", username, password).execute();
    	} else {
    		updateShoppingList(database.getShoppingList());
    	}
    }
    
    
    /**
     * Synchronisiert zwei Einkaufslisten miteinander. Dies ist zum einen die Liste die
     * vom Server geladen wurde und zum anderen die aktuelle Liste aus der lokalen
     * Datenbank.
     * @param _onlineList Die Einkaufsliste, die vom Server heruntergeladen wurde.
     */
    public void synchronize(ArrayList<Entry> _onlineList){
    	List<Entry> localList = shoppingList;
    	ArrayList<Entry> onlineList = _onlineList;

    	ArrayList<Entry> tmpOnline = new ArrayList<Entry>(onlineList);
    	ArrayList<Entry> del = new ArrayList<Entry>();
    	
    	//Jedes "abgehakte" Produkt identifizieren und vom Server l�schen
    	for(Entry el : localList){
    		if(el.isChecked()){
    			for(Entry eo : tmpOnline){
    				if(el.getBarcode().equals(eo.getBarcode())){
    					onlineList.remove(eo);
    					del.add(eo);
    				}
    			}
    		}
    	}    	
    	updateShoppingList(onlineList);    	
    	updateDatabase(onlineList);
    	
    	for(Entry d : del){    		
    		new ApiRequest(this, "", "deleteProductFromList", d.getBarcode(), username, password).execute();
    	}
    }
    
    
    /**
     * Aktualisiert die Einkaufsliste und bringt die GUI dazu,
     * diese erneut (aktuell) darzustellen.
     * @param _shoppingList Die aktuelle Einkaufsliste
     */
    private void updateShoppingList(ArrayList<Entry> _shoppingList){
    	shoppingList = _shoppingList;
    	arrayAdapter.clear();
    	arrayAdapter.addAll(shoppingList);  
    	shoppingListView.setAdapter(arrayAdapter);     	
    }  
    
    
    /**
     * Aktualisiert die lokale Datenbank.
     * @param _shoppingList Die aktuelle Einkaufsliste
     */
    private void updateDatabase(ArrayList<Entry> _shoppingList){
    	database.dropTable();
    	database.addProducts(_shoppingList);
    }
    
    
    /**
     * Startet den automatischen Aktualisierungsprozess der Einkaufsliste.
     */
    private void startAutomaticUpdateRoutine(){ 
    	updateInterval = preferences.getUpdatePeriod();
    	
    	automaticUpdateHandler.removeCallbacks(updateTask);
    	automaticUpdateHandler.postDelayed(updateTask, updateInterval);
    }
    

    /**
     * Stoppt den automatischen Aktualisierungsprozess der Einkaufsliste.
     */
    private void stopAutomaticUpdateRoutine(){    	
    	automaticUpdateHandler.removeCallbacks(updateTask);
    }
    
    
    /**
     * Der automatische Aktualisierungsprozess als Thread.
     */
    private Runnable updateTask = new Runnable(){
		public void run() {
			downloadShoppingList();
			automaticUpdateHandler.postDelayed(this, updateInterval);
		}    		
	}; 
    
    
	/**
	 * Reagiert wenn der Benutzer auf ein Produkt in der Einkaufsliste dr�ckt.
	 * In dem Fall wird eine neue Activity erzeugt und die notwendigen
	 * Daten (barcode, username, password) an diese �bergeben.
	 * @return Den entsprechenden Listener.
	 */
    private OnItemClickListener showDetailsListener(){
    	return new OnItemClickListener() {
        	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        		String barcode = shoppingList.get(position - 1).getBarcode();
        		
        		Intent intent = new Intent(ShoppingListModel.this, ProductDetailsModel.class);
        		intent.putExtra("barcode", barcode);
        		intent.putExtra("username", username);
        		intent.putExtra("password", password);
        		startActivity(intent);
        	}
        };
    }
    
    
    /**
     * Reagiert wenn der Benutzer manuell die Einkaufsliste aktualisieren will.
     * @return Den entsprechenden Listener.
     */
    private OnClickListener updateListListener(){
    	return new OnClickListener() {
            public void onClick(View v) {
            	downloadShoppingList();
            }
        };
    }
    
    
    /**
     * Reagiert wenn der Benutzer einen Barcode mit der integrierten
     * Kamera einscannen will.
     * @return Den entsprechenden Listener.
     */
    //TODO das darf nur gehen wenn man online ist!!!
    private OnClickListener scanBarcodeListener(){
    	return new OnClickListener(){            
            public void onClick(View v) {    	
            	/*
            	Intent intent = new Intent(ShoppingListModel.this, NewProductModel.class);
         	   	intent.putExtra("barcode", lastManuallyScannedBarcode);
         	   	intent.putExtra("username", username);
         	   	intent.putExtra("password", password);
         	   	startActivity(intent);
            	*/
            	
            	try{
	                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
	                intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
	                startActivityForResult(intent, 0);
            	} catch (ActivityNotFoundException e){
            		Toast.makeText(getApplicationContext(), "Zxing Barcode Scanner App nicht installiert. Scannen nicht m�glich.", 10000).show();
            	}
            	
            }
        };
    }
    
    
    /**
     * Pr�ft ob das Ger�t mit dem Internet verbunden ist.
     * @return true falls mit Internet verbunden, false sonst.
     */
    private boolean isConnected(){
    	ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    	
	    NetworkInfo networkInfo = cm.getActiveNetworkInfo();
	    
	    if (networkInfo != null && networkInfo.isConnected()) {
	    	return true;
	    }
	    return false;
    }    
    
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String barcode = intent.getStringExtra("SCAN_RESULT");
                
                if(isConnected()){
                	lastManuallyScannedBarcode = barcode;
                	new ApiRequest(this, "F�ge Produkt hinzu...", "addProductToList", barcode, username, password).execute();
                } else {
                	Toast.makeText(getApplicationContext(), "Dieses Feature ist nur im Online-Modus nutzbar.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
    
    
    @Override
    protected Dialog onCreateDialog(int id){
    	switch(id){
    		case 100:
		    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
		        builder.setMessage(R.string.unknownBarcodeInfo)
		               .setPositiveButton(R.string.insertNewProduct_Yes, new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                	   Intent intent = new Intent(ShoppingListModel.this, NewProductModel.class);
		                	   intent.putExtra("barcode", lastManuallyScannedBarcode);
		                	   intent.putExtra("username", username);
		                	   intent.putExtra("password", password);
		                	   startActivity(intent);
		                   }
		               })
		               .setNegativeButton(R.string.insertNewProduct_No, new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                	   Toast.makeText(getApplicationContext(), "Produkt nicht zur Einkaufsliste hinzugef�gt!", Toast.LENGTH_LONG).show();
		                   }
		               });		
		        AlertDialog dialog = builder.create();
		        dialog.show();
    	}
    	
    	return super.onCreateDialog(id);
    }
    
    
    /**
     * Registriert einen neuen BroadcastReceiver, der reagiert falls sich der
     * Verbindungsstatus des Ger�tes zum Internet �ndert.
     */
    private void registerConnectiviyListener(){
    	BroadcastReceiver connectivityListener = new BroadcastReceiver(){
			@Override
			public void onReceive(Context _context, Intent _intent) {
				if(isConnected()){
					networkStateTextView.setText("online");
					networkStateTextView.setTextColor(Color.parseColor("#009900"));
				} else {
					updateDatabase(shoppingList);
					
					networkStateTextView.setText("offline");
					networkStateTextView.setTextColor(Color.parseColor("#CC0000"));
				}
			}        	
        };
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);        
        this.registerReceiver(connectivityListener, filter);
    }
    
}

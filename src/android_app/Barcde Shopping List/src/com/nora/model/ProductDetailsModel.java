package com.nora.model;

import com.nora.R;
import com.nora.network.ApiRequest;
import com.nora.network.ImageRequest;
import com.nora.util.ProductDetails;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ProductDetailsModel extends Activity {
	
	//ampel-werte f�r lebensmittel
	private static final double fat_green = 3; 		//must be lower/equal
	private static final double satFat_green = 1.5;	//must be lower/equal
	private static final double sugar_green = 5;	//must be lower/equal
	private static final double salt_green = 0.3;	//must be lower/equal
	private static final double fat_red = 20;		//must be higher
	private static final double satFat_red = 5;		//must be higher
	private static final double sugar_red = 12.5;	//must be higher
	private static final double salt_red = 1.5;		//must be higher
	
	//ampel-werte f�r getr�nke
	private static final double drink_fat_green = 1.5;		//must be lower/equal
	private static final double drink_satFat_green = 0.75;	//must be lower/equal
	private static final double drink_sugar_green = 2.5;	//must be lower/equal
	private static final double drink_salt_green = 0.3;		//must be lower/equal
	private static final double drink_fat_red = 10;			//must be higher
	private static final double drink_satFat_red = 2.5;		//must be higher
	private static final double drink_sugar_red = 6.3;		//must be higher
	private static final double drink_salt_red = 1.5;		//must be higher
	

	private TextView tv_productName;
	private TextView tv_portion;
	private TextView tv_kcal;
	private TextView tv_kj;
	private TextView tv_trafficLightHeader;
	private TextView tv_fatValue;
	private TextView tv_satFatValue;
	private TextView tv_sugarValue;
	private TextView tv_saltValue;
	private TextView tv_Producer;

	private ImageView iv_productImage;

	private LinearLayout ll_main;
	private LinearLayout ll_calorification;
	private LinearLayout ll_trafficLights;
	private LinearLayout ll_fat;
	private LinearLayout ll_satFat;
	private LinearLayout ll_sugar;
	private LinearLayout ll_natrium;
	
	private String barcode;
	
	private String username;
	
	private String password;
	
	
	 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details);
                
        Bundle extras = getIntent().getExtras();        
        if(extras != null){
        	barcode = extras.getString("barcode");
        	username = extras.getString("username");
        	password = extras.getString("password");
        }
        
        tv_productName = (TextView) findViewById(R.id.tv_productName);    	
        tv_portion = (TextView) findViewById(R.id.tv_portion);
        tv_kcal = (TextView) findViewById(R.id.tv_kcal);
        tv_kj = (TextView) findViewById(R.id.tv_kj);
        tv_trafficLightHeader = (TextView) findViewById(R.id.tv_trafficLightHeader);
        tv_fatValue = (TextView) findViewById(R.id.tv_fatValue);
        tv_satFatValue = (TextView) findViewById(R.id.tv_satFatValue);
        tv_sugarValue = (TextView) findViewById(R.id.tv_sugarValue);
        tv_saltValue = (TextView) findViewById(R.id.tv_saltValue);
        tv_Producer = (TextView) findViewById(R.id.tv_Producer);

    	iv_productImage = (ImageView) findViewById(R.id.iv_productImage);
    	
    	ll_main = (LinearLayout) findViewById(R.id.ll_main);
    	ll_calorification = (LinearLayout) findViewById(R.id.ll_calorification);
    	ll_trafficLights = (LinearLayout) findViewById(R.id.ll_trafficLights);
    	ll_fat = (LinearLayout) findViewById(R.id.ll_fat);
    	ll_satFat = (LinearLayout) findViewById(R.id.ll_satFat);
    	ll_sugar = (LinearLayout) findViewById(R.id.ll_sugar);
    	ll_natrium = (LinearLayout) findViewById(R.id.ll_natrium);
        
    	ll_main.setVisibility(View.INVISIBLE);
    	
    	new ApiRequest(this, "Lade Produktdetails...", "getProductDetails", barcode, username, password).execute();    	
    }
    
    
    /**
     * Initialisiert alle Textfelder und Bilder der Detailansicht eines Produkts.
     * Wird aufgerufen durch den ResponseHandler.
     * @param _details Das Produkt, dessen Details angezeigt werden sollen.
     */
    public void init(ProductDetails _details){
    	int isDrink = _details.isDrink();
    	
    	double fat = _details.getFat();
    	double sat_fat = _details.getSatFat();
    	double sugar = _details.getSugar();
    	double salt = _details.getSalt();
    	
    	tv_productName.setText(_details.getName());
    	tv_kcal.setText(formatValue(_details.getKcal()) + " kcal");
    	tv_kj.setText(formatValue(_details.getKj()) + " kj");
    	tv_fatValue.setText(formatValue(fat) + "g");
    	tv_satFatValue.setText(formatValue(sat_fat) + "g");
    	tv_sugarValue.setText(formatValue(sugar) + "g");
    	tv_saltValue.setText(formatValue(salt) + "g");
    	
    	setTrafficLights(isDrink, fat, sat_fat, sugar, salt);
    	setCaptions(isDrink);
    	setOriginInfo(_details.getCompany(), _details.getCountry());
    	setProductImage(_details.getImage_path()); 
    	
    	ll_main.setVisibility(View.VISIBLE);
    }
    
    
    private String formatValue(double value){
    	return (value % 1 == 0) ? String.valueOf((int) value) : String.valueOf(value);
    }
    
    
    private void setCaptions(int isDrink){
    	switch(isDrink){
    		case 0:
    			tv_portion.setText("pro 100g");
        		tv_trafficLightHeader.setText("Lebensmittel-Ampel (je 100g):");
        		break;
    		case 1:
    			tv_portion.setText("pro 100ml");
        		tv_trafficLightHeader.setText("Lebensmittel-Ampel (je 100ml):");
        		break;
    		default:
    			tv_portion.setVisibility(View.INVISIBLE);
    			tv_trafficLightHeader.setVisibility(View.INVISIBLE);
    			tv_kcal.setVisibility(View.INVISIBLE);
    			tv_kj.setVisibility(View.INVISIBLE);
    			ll_calorification.setBackgroundDrawable(null);;
    			break;
    	}
    }
    
    
    private void setOriginInfo(String company, String country){    	
    	if(company.equalsIgnoreCase("") && country.equalsIgnoreCase("")){
    		SpannableString span = new SpannableString("keine Angabe");
    		span.setSpan(new StyleSpan(Typeface.ITALIC), 0, span.length(), 0);
    		tv_Producer.setText(span);
    	} else {
    		if(!company.equalsIgnoreCase("") && country.equalsIgnoreCase("")){
    			tv_Producer.setText(company);
    		} else {
    			if(company.equalsIgnoreCase("") && !country.equalsIgnoreCase("")){
    				tv_Producer.setText(country);
    			} else {
    				tv_Producer.setText(company + " (" + country + ")");
    			}
    		}
    	}
    }
    
    
    private void setProductImage(String url){
    	if(!url.equalsIgnoreCase("")){
    		new ImageRequest(this, "Lade Produktbild...").execute(url);
    	}
    }
    
    
    private void setTrafficLights(int isDrink, double fat, double sat_fat, double sugar, double salt){
    	switch(isDrink){
    		case 0:
    			setFoodLights(fat, sat_fat, sugar, salt);
    			break;
    		case 1:
    			setDrinkLights(fat, sat_fat, sugar, salt);
    			break;
    		default:
    			ll_trafficLights.setVisibility(View.INVISIBLE);
    			ll_trafficLights.removeAllViews();
    	}
    }
    
    
    private void setFoodLights(double fat, double sat_fat, double sugar, double salt){
    	if(fat <= fat_green){
    		ll_fat.setBackgroundResource(R.drawable.green_traffic_light);
    	} else {
    		if(fat > fat_red){
    			ll_fat.setBackgroundResource(R.drawable.red_traffic_light);
    		} else {
    			ll_fat.setBackgroundResource(R.drawable.yellow_traffic_light);
    		}
    	}
    	
    	if(sat_fat <= satFat_green){
    		ll_satFat.setBackgroundResource(R.drawable.green_traffic_light);
    	} else {
    		if(sat_fat > satFat_red){
    			ll_satFat.setBackgroundResource(R.drawable.red_traffic_light);
    		} else {
    			ll_satFat.setBackgroundResource(R.drawable.yellow_traffic_light);
    		}
    	}
    	
    	if(sugar <= sugar_green){
    		ll_sugar.setBackgroundResource(R.drawable.green_traffic_light);
    	} else {
    		if(sugar > sugar_red){
    			ll_sugar.setBackgroundResource(R.drawable.red_traffic_light);
    		} else {
    			ll_sugar.setBackgroundResource(R.drawable.yellow_traffic_light);
    		}
    	}
    	
    	if(salt <= salt_green){
    		ll_natrium.setBackgroundResource(R.drawable.green_traffic_light);
    	} else {
    		if(salt > salt_red){
    			ll_natrium.setBackgroundResource(R.drawable.red_traffic_light);
    		} else {
    			ll_natrium.setBackgroundResource(R.drawable.yellow_traffic_light);
    		}
    	}
    }
    
    
    private void setDrinkLights(double fat, double sat_fat, double sugar, double salt){
    	if(fat <= drink_fat_green){
    		ll_fat.setBackgroundResource(R.drawable.green_traffic_light);
    	} else {
    		if(fat > drink_fat_red){
    			ll_fat.setBackgroundResource(R.drawable.red_traffic_light);
    		} else {
    			ll_fat.setBackgroundResource(R.drawable.yellow_traffic_light);
    		}
    	}
    	
    	if(sat_fat <= drink_satFat_green){
    		ll_satFat.setBackgroundResource(R.drawable.green_traffic_light);
    	} else {
    		if(sat_fat > drink_satFat_red){
    			ll_satFat.setBackgroundResource(R.drawable.red_traffic_light);
    		} else {
    			ll_satFat.setBackgroundResource(R.drawable.yellow_traffic_light);
    		}
    	}
    	
    	if(sugar <= drink_sugar_green){
    		ll_sugar.setBackgroundResource(R.drawable.green_traffic_light);
    	} else {
    		if(sugar > drink_sugar_red){
    			ll_sugar.setBackgroundResource(R.drawable.red_traffic_light);
    		} else {
    			ll_sugar.setBackgroundResource(R.drawable.yellow_traffic_light);
    		}
    	}
    	
    	if(salt <= drink_salt_green){
    		ll_natrium.setBackgroundResource(R.drawable.green_traffic_light);
    	} else {
    		if(salt > drink_salt_red){
    			ll_natrium.setBackgroundResource(R.drawable.red_traffic_light);
    		} else {
    			ll_natrium.setBackgroundResource(R.drawable.yellow_traffic_light);
    		}
    	}
    }
    
    
    /**
     * L�dt das Produktbild in der Detailansicht.
     * @param _image Das Produktbild als Bitmap.
     */
    public void setProductImage(Bitmap _image){
    	if(_image != null){
    		iv_productImage.setImageBitmap(_image);
    	}
    }
        	
}

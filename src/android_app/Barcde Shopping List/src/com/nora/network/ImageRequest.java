package com.nora.network;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import com.nora.model.ProductDetailsModel;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

public class ImageRequest extends AsyncTask<String, Void, Bitmap> {
	
	//Ein ImageRequest kann und darf nur aus der Detailansicht eines Produkts erfolgen.
	private ProductDetailsModel productDetailsActivity;

	private ProgressDialog progressDialog;
	
	private String progressDialogMessage;
	
	
	
	public ImageRequest(ProductDetailsModel _productDetailsActivity, String _progressDialogMessage){
		productDetailsActivity = _productDetailsActivity;
		progressDialogMessage = _progressDialogMessage;
	}
	
	
	
	@Override
	protected void onPreExecute(){
		super.onPreExecute();

		progressDialog = new ProgressDialog(productDetailsActivity);
		if(!(progressDialogMessage == "")){
			progressDialog.setMessage(progressDialogMessage);
			progressDialog.show();
		}
	}
	
	
	@Override
	protected Bitmap doInBackground(String... _params) {			
		InputStream inputStream = null;
    	Bitmap productImage = null;
    	URLConnection urlConnection;
    	String urlString = _params[0];
    	
    	if(urlString.equalsIgnoreCase("")){
			try {
		    	URL url = new URL(urlString);
				urlConnection = url.openConnection();
				
	    		HttpURLConnection httpConn = (HttpURLConnection)urlConnection;
	    		httpConn.setRequestMethod("GET");
	    		httpConn.connect();
	    
	    		if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
	    			inputStream = httpConn.getInputStream(); 
	    		}    		
	
	    		productImage = BitmapFactory.decodeStream(inputStream);
	    		inputStream.close();
	    		
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
            	
    	return productImage; 
	}
	
	
	@Override
	protected void onPostExecute(Bitmap _response){
		if(_response != null){
			productDetailsActivity.setProductImage(_response);
		}
		
		if(progressDialog.isShowing()){
			progressDialog.dismiss();
		}
	}
	
}

package com.nora.network;

import java.lang.reflect.Type;
import java.util.ArrayList;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nora.model.LoginModel;
import com.nora.model.NewProductModel;
import com.nora.model.ProductDetailsModel;
import com.nora.model.ShoppingListModel;
import com.nora.util.Entry;
import com.nora.util.ProductDetails;

public class ResponseHandler {
	
	private Gson gson;
	
	private Activity activityContext;
	
	private static ResponseHandler responseHandler;
	
	
	/**
	 * Konstruktor. Erzeugt eine neue Instanz zur Google Gson-Bibliothek.
	 */
	public ResponseHandler(){
		gson = new Gson();
	}
	
	
	
	/**
	 * Gibt die Instanz des ResponseHandler zur�ck.
	 * Erzeugt eine neue Instanz falls noch keine existiert und
	 * gibt dann diese zur�ck.
	 * @return
	 */
	public static ResponseHandler getInstance(){
		synchronized (ResponseHandler.class){
			if(responseHandler == null){
				responseHandler = new ResponseHandler();
			}
		}
		return responseHandler;
	}
	
	
	/**
	 * Reagiert auf einkommende Antworten des Servers.
	 * @param _action Die Art der Anfrage die an den Server gestellt wurde.
	 * @param _response Die Antwort auf die Anfrage.
	 * @param _activityContext Der Kontext in welchem die Anfrage entstanden ist.
	 */
	public void handleResponse(String _action, String _response, Activity _activityContext){
		activityContext = _activityContext;
		
		if(_action.equalsIgnoreCase("getShoppingList")){
			parseShoppingListResponse(_response);
		} 
		else if(_action.equalsIgnoreCase("authenticate")){
			parseAuthenticationResponse(_response);
		}
		else if(_action.equalsIgnoreCase("getProductDetails")){
			parseProductDetailsResponse(_response);
		}
		else if(_action.equalsIgnoreCase("addProductToList")){
			parseAddProductToListResponse(_response);
		}
		else if(_action.equalsIgnoreCase("insertNewProduct")){
			parseNewProductResponse(_response);
		}
	}
	
	
	/**
	 * Behandelt einen Response der als Inhalt hat, ob der Benutzer sich erfolgreich
	 * am Server authentifiziert hat oder nicht.
	 * @param _response Die Antwort des Servers im JSON-Format.
	 */
	private void parseAuthenticationResponse(String _response){
		String s = gson.fromJson(_response, String.class);
		
		if(activityContext instanceof LoginModel){
			LoginModel login = (LoginModel) activityContext;

			if(s.equalsIgnoreCase("true")){
				login.onSuccess();
			} else {
				login.onFail(s);
			}
		}
	}
	
	
	/**
	 * Behandelt einen Response der als Inhalt die aktuelle Einkaufsliste des
	 * Benutzers enth�lt.
	 * @param _response Die Antwort des Servers im JSON-Format.
	 */
	private void parseShoppingListResponse(String _response){
		if(activityContext instanceof ShoppingListModel){
			ShoppingListModel shoppingList = (ShoppingListModel) activityContext;
						
			try{
				Type type = new TypeToken<ArrayList<Entry>>(){}.getType();		
				ArrayList<Entry> entries = gson.fromJson(_response, type);	
				
				shoppingList.synchronize(entries);
				
			} catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Behandelt einen Response der als Inhalt Detailinformationen zu 
	 * einem Produkt enth�lt.
	 * @param _response Die Antwort des Servers im JSON-Format.
	 */
	private void parseProductDetailsResponse(String _response){
		if(activityContext instanceof ProductDetailsModel){
			ProductDetailsModel productDetails = (ProductDetailsModel) activityContext;
			
			try{		
				ProductDetails details = gson.fromJson(_response, ProductDetails.class);			
				productDetails.init(details);
				
			} catch(Exception e){
				//TODO wenn kein objekt kommt, kommt ein fehler als String. ANZEIGEN !!!
				System.out.println("########### ERROR: " + e.getMessage());
			}
		}
	}
	
	
	private void parseAddProductToListResponse(String _response){
		Log.i("nora", _response);
		
		if(activityContext instanceof ShoppingListModel){
			ShoppingListModel shoppingList = (ShoppingListModel) activityContext;

			String s = gson.fromJson(_response, String.class);
			
			if(s != null && s.equalsIgnoreCase("Fehler: Unbekannter Barcode.")){
				shoppingList.showDialog(100);
			} else {
				shoppingList.downloadShoppingList();
			}
		}
	}
	
	
	private void parseNewProductResponse(String _response){
		if(activityContext instanceof NewProductModel){
			NewProductModel newProductModel = (NewProductModel) activityContext;

			String s = gson.fromJson(_response, String.class);
			
			if(s.equalsIgnoreCase("success")){
				newProductModel.finish();
				ShoppingListModel slm = (ShoppingListModel) ShoppingListModel.context;
				slm.downloadShoppingList();
			} else {
				//es ist ein fehler aufgetreten
			}
		}
	}
	
}

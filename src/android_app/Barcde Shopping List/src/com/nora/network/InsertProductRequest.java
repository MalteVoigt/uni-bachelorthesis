package com.nora.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

public class InsertProductRequest extends AsyncTask<Void, Void, String> {

	private ProgressDialog progressDialog;
	
	private Activity activity;
	
	private static final String apiUrl = "https://syncreal.uni-bremen.de/nora/index.php";
	
	private static final String progressDialogMessage = "Sende Anfrage an Server...";
	
	private static final String action = "insertNewProduct";
	
	private String username;
	private String password;
	
	private String barcode;
	private String isDrink;
	private String name;
	private String fat;
	private String satFat;
	private String sugar;
	private String salt;
	private String energy;
	private String image;
	private String imageName;


			
	public InsertProductRequest(Activity activity, String username, String password, 
								String barcode, int isDrink, String name, double fat, 
								double satFat, double sugar, double salt, double energy, 
								String image, String imageName){
		System.out.println("### Passwort in KOnstruktor(): " + password);
		this.activity = activity;
		
		this.username = username;
		this.password = password;
		
		this.barcode = barcode;
		this.name = name;
		this.isDrink = String.valueOf(isDrink);
		this.fat = String.valueOf(fat);
		this.satFat = String.valueOf(satFat);
		this.sugar = String.valueOf(sugar);
		this.salt = String.valueOf(salt);
		this.energy = String.valueOf(energy);
		this.image = image;
		this.imageName = imageName;
	}
	
	
	
	@Override
	protected void onPreExecute(){
		super.onPreExecute();

		progressDialog = new ProgressDialog(activity);
		if(!(progressDialogMessage == "")){
			progressDialog.setMessage(progressDialogMessage);
			progressDialog.show();
		}
	}
	
		
	@Override
	protected String doInBackground(Void... params) {	
		System.out.println("### Passwort in doInBackground(): " + password);	
		StringBuffer response = new StringBuffer();

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(apiUrl);
						
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	    nameValuePairs.add(new BasicNameValuePair("username", username));
	    nameValuePairs.add(new BasicNameValuePair("password", createMD5(password)));
	    nameValuePairs.add(new BasicNameValuePair("barcode", barcode));
	    nameValuePairs.add(new BasicNameValuePair("name", name));
	    nameValuePairs.add(new BasicNameValuePair("isDrink", isDrink));
	    nameValuePairs.add(new BasicNameValuePair("fat", fat));
	    nameValuePairs.add(new BasicNameValuePair("satFat", satFat));
	    nameValuePairs.add(new BasicNameValuePair("sugar", sugar));
	    nameValuePairs.add(new BasicNameValuePair("salt", salt));
	    nameValuePairs.add(new BasicNameValuePair("energy", energy));
	    nameValuePairs.add(new BasicNameValuePair("image", image));
	    nameValuePairs.add(new BasicNameValuePair("imageName", imageName));
	    
		try {
		    post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		    
		    HttpResponse httpResponse = client.execute(post);

            BufferedReader in = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

            String inputLine;

            while((inputLine = in.readLine()) != null){
            	response.append(inputLine);
            }
		    
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println(response.toString());
		
		return response.toString();
	}
	
	
	@Override
	protected void onPostExecute(String _response){
		if(progressDialog.isShowing()){
			progressDialog.dismiss();
		}
		
		ResponseHandler.getInstance().handleResponse(action, _response, activity);		
	}
	
	
	private String createMD5(String _string){
		System.out.println("### Passwort in createMD5(): " + _string);
		MessageDigest md5;
		byte[] hash = null;
		
		try {
			md5 = MessageDigest.getInstance("MD5");
	        md5.reset();
	        md5.update(_string.getBytes());
	        hash = md5.digest();
	        
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		        
        StringBuffer hexString = new StringBuffer();
        
        for (int i = 0; i < hash.length; i++) {
		   	String hex = Integer.toHexString(0xFF & hash[i]); 
		   	
		   	if(hex.length()==1){
		   		hexString.append('0');
		   	}		   	
		   	hexString.append(hex);
	    }
        
		return hexString.toString();
	}

}

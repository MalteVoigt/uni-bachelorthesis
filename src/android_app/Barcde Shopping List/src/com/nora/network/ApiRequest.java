package com.nora.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;


public class ApiRequest extends AsyncTask<Void, Void, String> {
		
	private String apiURL = "https://syncreal.uni-bremen.de/nora/index.php?";
	
	private ProgressDialog progressDialog;
	
	private Activity activity;
	
	private String action;	
	private String barcode;	
	private String username;	
	private String password;	
	private String progressDialogMessage;
	
	
	
	public ApiRequest(Activity _activity, String _progressDialogMessage, String _action, String _barcode, String _username, String _password){
		activity = _activity;
		action = _action;
		barcode = _barcode;
		username = _username;
		password = createMD5(_password);
		progressDialogMessage = _progressDialogMessage;
	}
	
	
	
	@Override
	protected void onPreExecute(){
		super.onPreExecute();

		progressDialog = new ProgressDialog(activity);
		if(!(progressDialogMessage == "")){
			progressDialog.setMessage(progressDialogMessage);
			progressDialog.show();
		}
	}
	
	
	@Override
	protected String doInBackground(Void... _params) {    			
		apiURL += "user=" + username + "&";
		apiURL += "pw=" + password + "&";
		apiURL += "barcode=" + barcode + "&";
		apiURL += "action=" + action;		
		
		return getWebContent(apiURL);
	}
	
	
	@Override
	protected void onPostExecute(String _response){
		ResponseHandler.getInstance().handleResponse(action, _response, activity);
		
		if(progressDialog.isShowing()){
			progressDialog.dismiss();
		}
	}
	
	
	private String getWebContent(String urlString){	
		URL url;
		InputStream inputStream = null;
    	URLConnection urlConnection;
    	StringBuffer response = new StringBuffer();
		
		try {
	    	url = new URL(urlString);
			urlConnection = url.openConnection();

    		HttpURLConnection httpConn = (HttpURLConnection)urlConnection;
    		httpConn.setRequestMethod("GET");
    		httpConn.connect();
    
    		if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
    			inputStream = httpConn.getInputStream(); 

    	        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
    	        	        
    	        String inputLine;	
    	
    	        while ((inputLine = in.readLine()) != null){
    	        	response.append(inputLine);
    	        }    	

        		in.close();
        		
        		inputStream.close();
    		}    	
    		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return response.toString();
	}
	
	
	private String createMD5(String _string){
		MessageDigest md5;
		byte[] hash = null;
		
		try {
			md5 = MessageDigest.getInstance("MD5");
	        md5.reset();
	        md5.update(_string.getBytes());
	        hash = md5.digest();
	        
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		        
        StringBuffer hexString = new StringBuffer();
        
        for (int i = 0; i < hash.length; i++) {
		   	String hex = Integer.toHexString(0xFF & hash[i]); 
		   	
		   	if(hex.length()==1){
		   		hexString.append('0');
		   	}		   	
		   	hexString.append(hex);
	    }
        
		return hexString.toString();
	}
	
}

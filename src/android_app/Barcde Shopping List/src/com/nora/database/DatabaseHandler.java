package com.nora.database;

import java.util.ArrayList;

import com.nora.util.Entry;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
	
	//Name der Datenbank
	private static final String DATABASE_NAME = "shoppingList.db";
	
	//Datenbank-Version
	private static final int DATABASE_VERSION = 1;
	
	//Tabelle "list"
	private static final String TABLE_LIST = "list";
	
	//Spalten der Tabelle "list"
	private static final String COLUMN_BARCODE = "barcode";
	private static final String COLUMN_PRODUCT_NAME = "name";
	private static final String COLUMN_COUNT = "count";
	private static final String COLUMN_CHECKED = "checked";
	
	//Statement um die Tabelle "list" zu erstellen
	private static final String CREATE_LIST_TABLE = "CREATE TABLE " + TABLE_LIST + "(" + COLUMN_BARCODE + " varchar(13) PRIMARY KEY," 
																					   + COLUMN_PRODUCT_NAME + " varchar(50) NOT NULL,"
																					   + COLUMN_COUNT + " INTEGER NOT NULL,"
																					   + COLUMN_CHECKED + " INTEGER)";
	
	

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}



	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_LIST_TABLE);
	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST);
		onCreate(db);
	}
	
	
	public void addProducts(ArrayList<Entry> _entries){
		for(Entry e : _entries){
			if(updateProduct(e) == 0){
				Log.i("nora", "updateProduct() is 0 - " + e.getName());
				addProduct(e);
			}			
		}
	}
	
	
	public void addProduct(Entry _entry){
		Log.i("nora", "addProduct() - " + _entry.getName());
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(COLUMN_BARCODE, _entry.getBarcode());
		values.put(COLUMN_PRODUCT_NAME, _entry.getName());
		values.put(COLUMN_COUNT, _entry.getCount());
		values.put(COLUMN_CHECKED, _entry.isChecked() ? 1 : 0);
		
		db.insert(TABLE_LIST, null, values);
		db.close();
	}
	
	
	public int updateProduct(Entry _entry){
		Log.i("nora", "updateProduct() - " + _entry.getName());
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(COLUMN_BARCODE, _entry.getBarcode());
		values.put(COLUMN_PRODUCT_NAME, _entry.getName());
		values.put(COLUMN_COUNT, _entry.getCount());
		values.put(COLUMN_CHECKED, _entry.isChecked());
		
		return db.update(TABLE_LIST, values, COLUMN_BARCODE + " = ?", new String[]{ _entry.getBarcode() });
	}
	
	
	public void truncateTable(){
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("TRUNCATE TABLE " + TABLE_LIST);
	}
	
	
	public void dropTable(){
		Log.i("nora", "dropTable()");
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST);
		onCreate(db);
	}
	
	
	public void deleteProduct(Entry _entry){
		SQLiteDatabase db = this.getWritableDatabase();
		
		db.delete(TABLE_LIST, COLUMN_BARCODE + " = ?", new String[]{ _entry.getBarcode() });
		db.close();
	}
	
	
	//TODO: unused
	public Entry getEntry(String _barcode){
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor cursor = db.query(TABLE_LIST, new String[]{ COLUMN_CHECKED }, COLUMN_BARCODE + " = ?", new String[] { _barcode }, null, null, null, null);

        Entry e = new Entry();
        
		if (cursor != null){
	        cursor.moveToFirst();
	        int a = cursor.getInt(0);
	        e.setChecked(a == 1 ? true : false);
		}
		
		return e;
	}
	
	
	public ArrayList<Entry> getShoppingList(){
		Log.i("nora", "-----------------------------");
		Log.i("nora", "##### getShoppingList() #####");
		ArrayList<Entry> shoppingList = new ArrayList<Entry>();
		
		String query = "SELECT * FROM " + TABLE_LIST;
		
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.rawQuery(query, null);
		
		if(c.moveToFirst()){
			do{
				Entry e = new Entry();
				
				e.setBarcode(c.getString(0));
				e.setName(c.getString(1));
				e.setCount(Integer.parseInt(c.getString(2)));
				e.setChecked(c.getInt(3) == 1 ? true : false);

				Log.i("nora", "Read Entry from DB: " + e.getBarcode() + ", " + e.getName() + ", " + e.getCount() + ", " + e.isChecked());
				
				shoppingList.add(e);				
			} while(c.moveToNext());
		}
		
		return shoppingList;
	}
	
}

package com.nora.util;

public class Entry {

	private String barcode;
	private String name;
	private String amount;
	private String unit;
	private int count;
		
	private boolean isChecked;
	
	private ProductDetails details;

	
	
	public Entry(String _name){
		name = _name;
	}
	
	
	
	public Entry() {}


	
	public String getBarcode() {
		return barcode;
	}
	
	public void setBarcode(String _barcode) {
		barcode = _barcode;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int _count) {
		count = _count;
	}
		
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	
	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean _isChecked) {
		isChecked = _isChecked;
	}

	


	public ProductDetails getDetails() {
		return details;
	}

	public void setDetails(ProductDetails details) {
		this.details = details;
	}
		
}

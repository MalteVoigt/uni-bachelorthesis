package com.nora.util;

public class ProductDetails {

	private String name;
	private String country;
	private String company;
	private int isDrink;
	
	private double fat;
	private double satFat;
	private double sugar;
	private double natrium;
	private double salt;
	private double kcal;
	private double kj;
	private String image_path;
	
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getCompany() {
		return company;
	}
	
	public void setCompany(String company) {
		this.company = company;
	}
	
	public int isDrink() {
		return isDrink;
	}
	
	public void setDrink(int isDrink) {
		this.isDrink = isDrink;
	}
	
	public double getFat() {
		return fat;
	}
	
	public void setFat(double fat) {
		this.fat = fat;
	}
	
	public double getSatFat() {
		return satFat;
	}
	
	public void setSatFat(double satFat) {
		this.satFat = satFat;
	}
	
	public double getSugar() {
		return sugar;
	}
	
	public void setSugar(double sugar) {
		this.sugar = sugar;
	}
	
	public double getNatrium() {
		return natrium;
	}
	
	public void setNatrium(double natrium) {
		this.natrium = natrium;
	}
	
	public double getSalt() {
		return salt;
	}
	
	public void setSalt(double salt) {
		this.salt = salt;
	}
	
	public double getKcal() {
		return kcal;
	}
	
	public void setKcal(double kcal) {
		this.kcal = kcal;
	}
	
	public double getKj() {
		return kj;
	}
	
	public void setKj(double kj) {
		this.kj = kj;
	}
	
	public String getImage_path() {
		return image_path;
	}
	
	public void setImage_path(String image_path) {
		this.image_path = image_path;
	}

}

package com.nora.util;

import android.widget.CheckBox;
import android.widget.TextView;

public class EntryChildViews {
	
	private TextView productNameTextView;
	
	private TextView productCountTextView;
	
	private CheckBox checkBox;
	
	

	public EntryChildViews(TextView _productNameTextView, TextView _productCountTextView, CheckBox _checkBox){
		productNameTextView = _productNameTextView;
		productCountTextView = _productCountTextView;
		checkBox = _checkBox;
	}



	public TextView getProductNameTextView() {
		return productNameTextView;
	}

	public void setSubTitleTextView(TextView _productNameTextView) {
		productNameTextView = _productNameTextView;
	}
	
	public TextView getProductCountTextView() {
		return productCountTextView;
	}

	public void setProductCountTextView(TextView _productCountTextView) {
		productCountTextView = _productCountTextView;
	}

	public CheckBox getCheckBox() {
		return checkBox;
	}

	public void setCheckBox(CheckBox checkBox) {
		checkBox = checkBox;
	}
	
}

package com.nora.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Preferences {
	
	private static final String preferencesFileName = "noraPreferences";
	
	private static final String updatePeriodKey = "update_period";
	
	private static final String saveUserDataKey = "save_userdata";
	
	private static final String automaticLoginKey = "automatic_login";
	
	private static final String lastActiveUserKey = "last_active_user";
	
	private static final String lastActiveUserPasswordKey = "last_active_user_password";

	private SharedPreferences preferences;
	
	private Editor preferencesEditor;
	
	
	
	public Preferences(Context _context){
		preferences = _context.getSharedPreferences(preferencesFileName, 0);
		preferencesEditor = preferences.edit();
	}
	
	
	
	public long getUpdatePeriod(){
		//Aktualisierungs-Interval ist per Default 30 Minuten (1800000ms)
		return preferences.getLong(updatePeriodKey, 1800000);
	}
	
	
	public void setUpdatePeriod(long _period){
		preferencesEditor.putLong(updatePeriodKey, _period);
		preferencesEditor.commit();
	}
	
	
	public boolean getLoginState(){
		//Automatischer Login ist per Default false
		return preferences.getBoolean(automaticLoginKey, false);
	}
	
	
	public void setLoginState(boolean _isAutomatic){
		preferencesEditor.putBoolean(automaticLoginKey, _isAutomatic);
		preferencesEditor.commit();
	}
	
	
	public boolean getUserdataState(){
		//Benutzerdaten werden per Default gespeichert
		return preferences.getBoolean(saveUserDataKey, true);
	}
	
	
	public void setUserdataState(boolean _doSave){
		preferencesEditor.putBoolean(saveUserDataKey, _doSave);
		preferencesEditor.commit();
	}
	
	
	public String getLastActiveUser(){
		return preferences.getString(lastActiveUserKey, "");
	}
	
	
	public void setLastActiveUser(String _username){
		preferencesEditor.putString(lastActiveUserKey, _username);
		preferencesEditor.commit();
	}
	
	
	public String getLastActiveUserPassword(){
		return preferences.getString(lastActiveUserPasswordKey, "");
	}
	
	
	public void setLastActiveUserPassword(String _password){
		preferencesEditor.putString(lastActiveUserPasswordKey, _password);
		preferencesEditor.commit();
	}
	
}

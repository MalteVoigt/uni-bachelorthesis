package com.nora.util;

import java.util.List;

import com.nora.R;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class EntryArrayAdapter extends ArrayAdapter<Entry>{
	
	private static final String checkedItemColor = "#B0B0B0";

	private static final String uncheckedItemColor = "#000000";
	
	private LayoutInflater inflater;
	
	private List<Entry> listEntries;

	private CheckBox checkBox;
	
	private TextView productNameTextView;
	
	private TextView productCountTextView;
	
	

	public EntryArrayAdapter(Context _context, List<Entry> _listEntries){
		super(_context, R.layout.list_entry, _listEntries);
		inflater = LayoutInflater.from(_context);
		
		listEntries = _listEntries;
	}
	
	
	
	@Override
	public View getView(int _position, View _convertView, ViewGroup _parent){
		Entry listEntry = (Entry) this.getItem(_position);
				
		if(_convertView == null){
			_convertView = inflater.inflate(R.layout.list_entry, null);
						
			productNameTextView = (TextView) _convertView.findViewById(R.id.productNameTextView);  
			productCountTextView = (TextView) _convertView.findViewById(R.id.productCountTextView); 
	        checkBox = (CheckBox) _convertView.findViewById(R.id.rowCheckBox);
	        
	        _convertView.setTag(new EntryChildViews(productNameTextView, productCountTextView, checkBox));
	        
		} else {
			EntryChildViews childViews = (EntryChildViews) _convertView.getTag();
			
			checkBox = childViews.getCheckBox();
			productNameTextView = childViews.getProductNameTextView();
			productCountTextView = childViews.getProductCountTextView();
		}
		
		_convertView.setClickable(false);
		
		String productName = listEntry.getName();
		
		productNameTextView.setText(productName);
		productCountTextView.setText(String.valueOf(listEntry.getCount()));
				
		checkBox.setOnCheckedChangeListener(changeCheckStateListener(listEntry, productNameTextView, productCountTextView));
						
		if(listEntry.isChecked()){
			checkBox.setChecked(true);
			checkEntry(listEntry, productNameTextView, productCountTextView);
		} else {
			checkBox.setChecked(false);
			uncheckEntry(listEntry, productNameTextView, productCountTextView);
		}
				
		return _convertView;
	}
	
	
	private OnCheckedChangeListener changeCheckStateListener(final Entry _entry, final TextView _productName, final TextView _count){
		return new OnCheckedChangeListener(){
			public void onCheckedChanged(CompoundButton view, boolean isChecked) {
				if(isChecked){
					checkEntry(_entry, _productName, _count);
				} else {
					uncheckEntry(_entry, _productName, _count);
				}
			}			
		};
	}
	
	
	private void checkEntry(Entry _entry, TextView _productName, TextView _count){
		_productName.setPaintFlags(_productName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		_productName.setTextColor(Color.parseColor(checkedItemColor));						
		_count.setPaintFlags(_count.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		_count.setTextColor(Color.parseColor(checkedItemColor));
		
		_entry.setChecked(true);
	}
	
	
	private void uncheckEntry(Entry _entry, TextView _productName, TextView _count){
		_productName.setPaintFlags(_productName.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
		_productName.setTextColor(Color.parseColor(uncheckedItemColor));				
		_count.setPaintFlags(_count.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
		_count.setTextColor(Color.parseColor(uncheckedItemColor));
		
		_entry.setChecked(false);
	}
	
	
	public List<Entry> getItemList(){
		return listEntries;
	}
	
}
